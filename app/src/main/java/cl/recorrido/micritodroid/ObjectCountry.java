package cl.recorrido.micritodroid;

import java.util.ArrayList;

/**
 * Created by brede on 05-11-14.
 */
public class ObjectCountry {
    int CountryId;
    String CountryName;
    ArrayList<ObjectCity> ciudades;

    public ArrayList<ObjectCity> getCiudades() {
        return ciudades;
    }

    public void setCiudades(ArrayList<ObjectCity> ciudades) {
        this.ciudades = ciudades;
    }

    public int getCountryId() {
        return CountryId;
    }

    public void setCountryId(int countryId) {
        CountryId = countryId;
    }

    public String getCountryName() {
        return CountryName;
    }

    public void setCountryName(String countryName) {
        CountryName = countryName;
    }

    public ObjectCountry() {

    }

    public ObjectCountry(int countryId, String countryName) {

        CountryId = countryId;
        CountryName = countryName;
    }
}
