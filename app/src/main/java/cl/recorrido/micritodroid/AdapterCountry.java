
package cl.recorrido.micritodroid;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import java.util.ArrayList;

public class AdapterCountry extends ArrayAdapter<ObjectCountry> {

    Context context;
    int layoutResourceId;
    ArrayList<ObjectCountry> data = null;


    public AdapterCountry(Context context, int layoutResourceId, ArrayList<ObjectCountry> data) {
        super(context, layoutResourceId, data);
        this.layoutResourceId = layoutResourceId;
        this.context = context;
        this.data = data;
    }

    public void changeData(ArrayList<ObjectCountry> data) {
        this.data = data;
        notifyDataSetChanged();
    }


    ContainerCountry holder = null;
    View row;

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        row = convertView;
        final ObjectCountry object = data.get(position);
        if (row == null) {
            LayoutInflater inflater = ((ActivityMain) context).getLayoutInflater();
            row = inflater.inflate(R.layout.extra_fila_city, null);
            holder = new ContainerCountry();
            holder.name = (TextView) row.findViewById(R.id.name);
            holder.id = (TextView) row.findViewById(R.id.idcity);
            row.setTag(holder);
        } else {
            holder = (ContainerCountry) row.getTag();
        }
        holder.name.setText(object.getCountryName());
        holder.id.setText(String.valueOf(object.getCountryId()));


        return row;
    }

    static class ContainerCountry {
        TextView name;
        TextView id;

    }
}
