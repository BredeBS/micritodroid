package cl.recorrido.micritodroid;

/**
 * Created by brede on 03-11-14.
 */
public class ObjectCity {
    int id;
    String name;


    public ObjectCity(int id) {
        this.id = id;
    }

    public ObjectCity() {
    }

    public ObjectCity(int id, String name) {
        this.id = id;
        this.name = name;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
