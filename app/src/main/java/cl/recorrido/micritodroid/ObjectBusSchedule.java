package cl.recorrido.micritodroid;

import java.util.Date;

/**
 * Created by brede on 04-11-14.
 */
public class ObjectBusSchedule {
    int id;
    ObjectBusOperator BusOperator;
    Date Departure;
    Date Arrives;
    int Price;
    int SeatClassStars;
    int TravelTimeMinutes;
    int Type;
    String seat_klass;
    String TerminalDeparture;

    public String getTerminalArrival() {
        return TerminalArrival;
    }

    public void setTerminalArrival(String terminalArrival) {
        TerminalArrival = terminalArrival;
    }

    public String getTerminalDeparture() {
        return TerminalDeparture;
    }

    public void setTerminalDeparture(String terminalDeparture) {
        TerminalDeparture = terminalDeparture;
    }

    public String getSeat_klass() {
        return seat_klass;
    }

    public void setSeat_klass(String seat_klass) {
        this.seat_klass = seat_klass;
    }

    String TerminalArrival;

    public int getType() {
        return Type;
    }

    public void setType(int type) {
        Type = type;
    }

    String ResourceUrl;

    public int getTravelTimeMinutes() {
        return TravelTimeMinutes;
    }

    public void setTravelTimeMinutes(int travelTimeMinutes) {
        TravelTimeMinutes = travelTimeMinutes;
    }

    public ObjectBusSchedule(int id, ObjectBusOperator BusOperator, Date departure, Date arrives, int price, int seatClassStars, int travelTimeMinutes, String resourceUrl) {

        this.id = id;
        this.BusOperator = BusOperator;
        Departure = departure;
        Arrives = arrives;
        Price = price;
        SeatClassStars = seatClassStars;
        TravelTimeMinutes = travelTimeMinutes;
        ResourceUrl = resourceUrl;
    }

    public int getId() {

        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public ObjectBusOperator getBusOperator() {
        return BusOperator;
    }

    public void setBusOperator(ObjectBusOperator BusOperator) {
        this.BusOperator = BusOperator;
    }

    public Date getDeparture() {
        return Departure;
    }

    public void setDeparture(Date departure) {
        Departure = departure;
    }

    public Date getArrives() {
        return Arrives;
    }

    public void setArrives(Date arrives) {
        Arrives = arrives;
    }

    public int getPrice() {
        return Price;
    }

    public void setPrice(int price) {
        Price = price;
    }

    public int getSeatClassStars() {
        return SeatClassStars;
    }

    public void setSeatClassStars(int seatClassStars) {
        SeatClassStars = seatClassStars;
    }

    public String getResourceUrl() {
        return ResourceUrl;
    }

    public void setResourceUrl(String resourceUrl) {
        ResourceUrl = resourceUrl;
    }

    public ObjectBusSchedule() {

    }
}
