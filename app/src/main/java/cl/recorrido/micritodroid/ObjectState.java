package cl.recorrido.micritodroid;

/**
 * Created by brede on 03-11-14.
 */
public class ObjectState {
    private boolean state;
    private String code;
    private int Progress;

    public int getProgress() {
        return Progress;
    }

    public void setProgress(int progress) {
        Progress = progress;
    }

    boolean ConnectionAvailable;

    public boolean isConnectionAvailable() {
        return ConnectionAvailable;
    }

    public void setConnectionAvailable(boolean isConnectionAvailable) {
        this.ConnectionAvailable = isConnectionAvailable;
    }

    public int getConnectionStateLocalMessageCode() {
        return ConnectionStateLocalMessageCode;
    }

    public void setConnectionStateLocalMessageCode(int ConnectionStateLocalMessageCode) {
        this.ConnectionStateLocalMessageCode = ConnectionStateLocalMessageCode;
    }

    private int ConnectionStateLocalMessageCode;
    private boolean keepWaiting = false;
    private int numberJustInCase;

    public int getAnotherNumberJustInCase() {
        return AnotherNumberJustInCase;
    }

    public void setAnotherNumberJustInCase(int AnotherNumberJustInCase) {
        AnotherNumberJustInCase = AnotherNumberJustInCase;
    }

    private int AnotherNumberJustInCase;

    public int getNumberJustInCase() {
        return numberJustInCase;
    }

    public void setNumberJustInCase(int numberJustInCase) {
        this.numberJustInCase = numberJustInCase;
    }

    public boolean isKeepWaiting() {
        return keepWaiting;
    }

    public void setKeepWaiting(boolean keepWaiting) {
        this.keepWaiting = keepWaiting;
    }

    public ObjectState() {
    }

    public boolean isState() {
        return state;
    }

    public void setState(boolean state) {
        this.state = state;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public ObjectState(boolean state, String code) {

        this.state = state;
        this.code = code;
    }
}
