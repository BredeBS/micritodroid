
package cl.recorrido.micritodroid;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;


import java.util.ArrayList;

public class AdapterCity extends ArrayAdapter<ObjectCity> {

    Context context;
    int layoutResourceId;
    ArrayList<ObjectCity> data = null;

    public AdapterCity(Context context, int layoutResourceId, ArrayList<ObjectCity> data) {
        super(context, layoutResourceId, data);
        this.layoutResourceId = layoutResourceId;
        this.context = context;
        this.data = data;
    }

    public void changeData(ArrayList<ObjectCity> data) {
        this.data = data;
        notifyDataSetChanged();
    }

    ContainerCity holder = null;
    View row;

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        row = convertView;
        final ObjectCity object = data.get(position);
        if (row == null) {
            LayoutInflater inflater = ((ActivityMain) context).getLayoutInflater();
            row = inflater.inflate(R.layout.extra_fila_city, null);
            holder = new ContainerCity();
            holder.name = (TextView) row.findViewById(R.id.name);
            holder.id = (TextView) row.findViewById(R.id.idcity);
            row.setTag(holder);
        } else {
            holder = (ContainerCity) row.getTag();
        }
        holder.name.setText(object.getName());
        holder.id.setText(String.valueOf(object.getId()));


        return row;
    }

    static class ContainerCity {
        TextView name;
        TextView id;

    }
}
