package cl.recorrido.micritodroid;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.graphics.Typeface;
import android.graphics.drawable.Drawable;
import android.os.Handler;
import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.AbsListView;
import android.widget.AdapterView;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.ImageLoaderConfiguration;

import java.util.ArrayList;


public class ActivityResults extends ActionBarActivity {


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_results);

    }

//    public void LoadOperatorSchedules() {
//        runOnUiThread(new Runnable() {
//
//            @Override
//            public void run() {
//                if (progressDialog == null) {
//                    progressDialog = new ProgressDialog(ActivityResults.this);
//                    progressDialog.setMessage(getBaseContext().getString(R.string.ordering));
//                    progressDialog.show();
//                    progressDialog.setCanceledOnTouchOutside(false);
//                    progressDialog.setCancelable(false);
////            CityDestiny.setText(R.string.Select);
//                }
//            }
//        });
//        if (intRating == 1 || intRating == 2) {
//            TextViews(txtRate, txtDepartureTime, txtFare);
//            Images(imgRate, imgDepartureTime, imgFare, intRating);
//        } else if (intDepartureTime == 1 || intDepartureTime == 2) {
//            TextViews(txtDepartureTime, txtRate, txtFare);
//            Images(imgDepartureTime, imgRate, imgFare, intDepartureTime);
//        } else if (intFare == 1 || intFare == 2) {
//            TextViews(txtFare, txtDepartureTime, txtRate);
//            Images(imgFare, imgDepartureTime, imgRate, intFare);
//        }
//
//        travel = db.getTravel(TravelID, TravelType, intRating, intDepartureTime, intFare);
//        if (travel.size() > 0) {
//            SchedulesList = (ListView) findViewById(R.id.listSchedules);
//
//            SchedulesList.setClickable(false);
//
////        db = new DB(this);
//
//            departureDate.setText(u.DateTimeToString(travel.getDepartureDate(), "EEEE d 'de' MMMM 'de' yyyy"));
//
//
//            ScheduleAdapter = new AdapterBusSchedule(this, R.layout.extra_row_country, travel.getSchedule());
//            SchedulesList.setAdapter(ScheduleAdapter);
//            SchedulesList.setChoiceMode(AbsListView.CHOICE_MODE_SINGLE);
//            SchedulesList.setOnItemClickListener(new AdapterView.OnItemClickListener() {
//                @Override
//                public void onItemClick(AdapterView<?> parent, View view, int position,
//                                        long id) {
//                    if (travel.isRoundTrip() && TravelType.compareTo("outbound") == 0) {
//                        SharedPreferences settings = getSharedPreferences("cl.recorrido.micritodroid", Context.MODE_PRIVATE);
//                        SharedPreferences.Editor editor = settings.edit();
//                        ObjectBusSchedule Schedule = travel.getSchedule().get(position);
//                        editor.putString("TravelType", "inbound");
//                        if (TravelType.compareTo("outbound") == 0) {
//                            editor.putString("outboundTravelID", String.valueOf(Schedule.getId()));
//                        } else {
//                            editor.putString("inboundTravelID", String.valueOf(Schedule.getId()));
//                        }
//                        editor.commit();
//                        Intent myIntent = new Intent(ActivityResults.this, ActivityResults.class);
//                        startActivity(myIntent);
//                    } else {
//
//                    }
//                }
//            });
//            runOnUiThread(new Runnable() {
//
//                @Override
//                public void run() {
//                    if (progressDialog.isShowing()) {
//                        progressDialog.dismiss();
//                        progressDialog = null;
//                    }
//                }
//            });
//        } else {
//            this.Message(getBaseContext().getText(R.string.MisteriousError).toString());
//        }
//    }


//    @Override
//    public boolean onCreateOptionsMenu(Menu menu) {
//        // Inflate the menu; this adds items to the action bar if it is present.
//        getMenuInflater().inflate(R.menu.menu_activity_results, menu);
//        return true;
//    }
//
//    @Override
//    public boolean onOptionsItemSelected(MenuItem item) {
//        // Handle action bar item clicks here. The action bar will
//        // automatically handle clicks on the Home/Up button, so long
//        // as you specify a parent activity in AndroidManifest.xml.
//        int id = item.getItemId();
//
//        //noinspection SimplifiableIfStatement
//        if (id == R.id.action_settings) {
//            return true;
//        }
//
//        return super.onOptionsItemSelected(item);
//    }
}
