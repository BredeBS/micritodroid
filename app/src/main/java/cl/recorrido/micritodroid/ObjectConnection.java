package cl.recorrido.micritodroid;

import org.apache.http.HttpResponse;

/**
 * Created by brede on 07-11-14.
 */
public class ObjectConnection {
    ObjectState state;
    HttpResponse HttpResponse;

    public ObjectState getState() {
        return state;
    }

    public void setState(ObjectState state) {
        this.state = state;
    }

    public HttpResponse getHttpResponse() {
        return HttpResponse;
    }

    public void setHttpResponse(HttpResponse httpResponse) {
        HttpResponse = httpResponse;
    }
}
