
package cl.recorrido.micritodroid;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import java.util.ArrayList;

public class AdapterBusSchedule extends ArrayAdapter<ObjectBusSchedule> {
    Util u;
    Context context;
    int layoutResourceId;
    ArrayList<ObjectBusSchedule> data = null;

    public AdapterBusSchedule(Context context, int layoutResourceId, ArrayList<ObjectBusSchedule> data) {
        super(context, layoutResourceId, data);
        this.layoutResourceId = layoutResourceId;
        this.context = context;
        this.data = data;
    }

    public void changeData(ArrayList<ObjectBusSchedule> data) {
        this.data = data;
        notifyDataSetChanged();
    }

    ContainerSchedule holder = null;
    View row;

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        row = convertView;
        u = new Util();
        final ObjectBusSchedule object = data.get(position);
        if (row == null) {
            LayoutInflater inflater = ((ActivityResults) context).getLayoutInflater();
            row = inflater.inflate(R.layout.extra_row_busschedule, null);
            holder = new ContainerSchedule();
            holder.txtDepartureTime = (TextView) row.findViewById(R.id.txtDepartureTime);
            holder.txtArrivalTime = (TextView) row.findViewById(R.id.txtArrivalTime);
            holder.txtPrice = (TextView) row.findViewById(R.id.txtPrice);
            holder.txtBusCompany = (TextView) row.findViewById(R.id.txtBusCompany);
            holder.txtTravelTime = (TextView) row.findViewById(R.id.txtTravelTime);
            holder.txtBusType = (TextView) row.findViewById(R.id.txtBusType);
            row.setTag(holder);
        } else {
            holder = (ContainerSchedule) row.getTag();
        }
        holder.txtDepartureTime.setText(u.DateTimeToString(object.getDeparture(), "H:mm"));
        holder.txtArrivalTime.setText(u.DateTimeToString(object.getArrives(), "H:mm"));
//        holder.txtArrivalTime.setText(String.valueOf(object.getCountryId()));
        holder.txtPrice.setText(u.FormatMoney(object.getPrice()));
        holder.txtBusCompany.setText(object.getBusOperator().getNameBusOperator());
        holder.txtTravelTime.setText(String.valueOf(u.MinutesToHours(object.getTravelTimeMinutes())));
        holder.txtBusType.setText(context.getString(u.BusType(object.getSeatClassStars())));


        return row;
    }

    static class ContainerSchedule {
        TextView txtDepartureTime;
        TextView txtArrivalTime;
        TextView txtPrice;
        TextView txtBusCompany;
        TextView txtTravelTime;
        TextView txtBusType;

    }
}
