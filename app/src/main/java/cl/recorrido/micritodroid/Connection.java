package cl.recorrido.micritodroid;

import android.content.Context;
import android.content.SharedPreferences;
import android.util.Log;

import org.apache.http.HttpResponse;
import org.apache.http.HttpVersion;
import org.apache.http.NameValuePair;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.conn.ClientConnectionManager;
import org.apache.http.conn.HttpHostConnectException;
import org.apache.http.conn.scheme.PlainSocketFactory;
import org.apache.http.conn.scheme.Scheme;
import org.apache.http.conn.scheme.SchemeRegistry;
import org.apache.http.conn.ssl.SSLSocketFactory;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.impl.conn.tsccm.ThreadSafeClientConnManager;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.params.BasicHttpParams;
import org.apache.http.params.HttpParams;
import org.apache.http.params.HttpProtocolParams;
import org.apache.http.protocol.HTTP;
import org.apache.http.util.EntityUtils;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.security.KeyStore;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

/**
 * Created by Brede on 28-04-14.
 */
public class Connection {

    DB db;


    public static HttpClient getNewHttpClient() {
        try {
            KeyStore trustStore = KeyStore.getInstance(KeyStore.getDefaultType());
            trustStore.load(null, null);

            SSLSocketFactory sf = new RecorridoSSL(trustStore);
            sf.setHostnameVerifier(SSLSocketFactory.ALLOW_ALL_HOSTNAME_VERIFIER);

            HttpParams params = new BasicHttpParams();
            HttpProtocolParams.setVersion(params, HttpVersion.HTTP_1_1);
            HttpProtocolParams.setContentCharset(params, HTTP.UTF_8);

            SchemeRegistry registry = new SchemeRegistry();
            registry.register(new Scheme("http", PlainSocketFactory.getSocketFactory(), 80));
            registry.register(new Scheme("https", sf, 443));

            ClientConnectionManager ccm = new ThreadSafeClientConnManager(params, registry);

            return new DefaultHttpClient(ccm, params);
        } catch (Exception e) {
            return new DefaultHttpClient();
        }
    }

    public ObjectConnection ConnectionGet(Context context, String url) throws IOException {
        Util u = new Util();
        ObjectConnection objectConnection = new ObjectConnection();
        ObjectState state = new ObjectState();
        try {
            if (u.isNetworkAvailable(context)) {
                HttpClient httpclient = this.getNewHttpClient();
                HttpGet httpGet = new HttpGet(url);
                HttpResponse response = httpclient.execute(httpGet);

                int code = response.getStatusLine().getStatusCode();
                state.setCode(String.valueOf(code));
                state.setConnectionStateLocalMessageCode(u.KeyValueError(code));
                if (code > 199 && code < 300) {
                    //success
                    state.setState(true);
                    objectConnection.setHttpResponse(response);
                } else if (code > 399 && code < 500) {
                    //client error
                    state.setState(false);
                } else if (code > 499 && code < 600) {
                    //server error
                    state.setState(false);
                } else {
                    //any other error
                    state.setState(false);
                }
                state.setConnectionAvailable(true);
            } else {
                state.setState(false);
                state.setConnectionStateLocalMessageCode(R.string.ConnectionError);
                state.setConnectionAvailable(false);
            }
        } catch (HttpHostConnectException e) {
            state.setState(false);
            state.setConnectionStateLocalMessageCode(R.string.ConnectionError);
            state.setConnectionAvailable(false);
        } catch (IOException e) {
            Log.v("IOEXception", "");
            e.getMessage();
            e.printStackTrace();
            state.setState(false);
            state.setConnectionStateLocalMessageCode(R.string.IOException);
        }
        objectConnection.setState(state);
        return objectConnection;
    }

    public ObjectState FirstLoad(Context context, String base) throws ClientProtocolException, IOException {
        ObjectState es = new ObjectState();
        es.setState(true);
        try {
            db = new DB(context);
            db.DeleteAllFromTable(db.getTableCities());
            db.DeleteAllFromTable(db.getTableCountry());
            db.DeleteAllFromTable(db.getTableBusOperator());
            db.DeleteAllFromTable(db.getTableDestiny());
            ObjectConnection objectConnection = this.ConnectionGet(context, base + "en/infos.json");
            if (objectConnection.getState().isConnectionAvailable()) {

                try {
                    if (Integer.parseInt(objectConnection.getState().getCode()) == 200) {
                        String responseJson = EntityUtils.toString(objectConnection.getHttpResponse().getEntity());
                        if (responseJson != null) {
                            JSONObject jsonObj = new JSONObject(responseJson);
                            this.FillCities(jsonObj.getJSONArray("cities"));
                            this.FillCountry(jsonObj.getJSONArray("countries"));
                            this.FillBusOperator(jsonObj.getJSONArray("bus_operators"));
                            es.setState(true);
                        }
                    } else {
                        es.setState(false);
                        es.setCode(String.valueOf(objectConnection.getHttpResponse().getStatusLine().getStatusCode()));
                    }
                } catch (JSONException e) {
                    Log.v("JSONException", "");
                    e.getMessage();
                    e.printStackTrace();
                }

            } else {
                es = objectConnection.getState();
            }


        } catch (IOException e) {
            Log.v("IOEXception", "");
            e.getMessage();
            e.printStackTrace();
        }


        return es;
    }

    public ObjectState Destiny(Context contexto, String base, String cityCode) throws ClientProtocolException, IOException {
        ObjectState es = new ObjectState();
        try {
            db = new DB(contexto);
            HttpClient httpclient = this.getNewHttpClient();
            Log.v("url", "Destiny");
            HttpGet httppost = new HttpGet(base + "en/cities/" + cityCode + ".json?minimal=true");
            HttpResponse response;

            response = httpclient.execute(httppost);
            String respuesta = EntityUtils.toString(response.getEntity());
            Log.v("response", respuesta);
            try {
                if (response.getStatusLine().getStatusCode() == 200) {
                    if (respuesta != null) {
                        JSONObject jsonObj = new JSONObject(respuesta);

                        this.DestinyProcess(jsonObj.getJSONArray("destination_city_ids"), cityCode);
                        es.setState(true);
                    }
                } else {
                    es.setState(false);
                    es.setCode(String.valueOf(response.getStatusLine().getStatusCode()));
                }
            } catch (JSONException e) {
                Log.v("JSONException", "");
                e.getMessage();
                e.printStackTrace();
            }
        } catch (IOException e) {
            Log.v("IOEXception", "");
            e.getMessage();
            e.printStackTrace();
        }


        return es;
    }

    //    this method allow to load all the base data from the API
    public void FillCities(JSONArray array) {
        try {
            int total = array.length();
            if (total > 0) {
                String query = "INSERT INTO " + db.getTableCities();
                JSONObject object = array.getJSONObject(0);
                query = query.concat(" SELECT '").concat(object.getString("id")).concat("' AS 'id', '").concat(object.getString("name")).concat("' AS 'name',  '").concat(object.getString("country_id")).concat("' AS 'city_country_id'");
                for (int j = 1; j < total; j++) {
                    object = array.getJSONObject(j);
                    query = query.concat(" UNION SELECT '").concat(object.getString("id")).concat("', '").concat(object.getString("name")).concat("',  '").concat(object.getString("country_id")).concat("'");
                }
                Log.v("query", query);
                db.Execute(query);
            }


        } catch (NullPointerException e) {
            e.printStackTrace();
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    public void FillCountry(JSONArray array) {
        try {
            int total = array.length();
            if (total > 0) {
                String query = "INSERT INTO " + db.getTableCountry();
                JSONObject object = array.getJSONObject(0);
                query = query.concat(" SELECT '").concat(object.getString("id")).concat("' AS 'country_id', '").concat(object.getString("official_name")).concat("' AS 'country_name' ");
                for (int j = 1; j < total; j++) {
                    object = array.getJSONObject(j);
                    query = query.concat(" UNION SELECT '").concat(object.getString("id")).concat("', '").concat(object.getString("official_name")).concat("'");
                }
                Log.v("query", query);
                db.Execute(query);
            }


        } catch (NullPointerException e) {
            e.printStackTrace();
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    public void FillBusOperator(JSONArray array) {
        try {
            int total = array.length();
            if (total > 0) {
                String query = "INSERT INTO " + db.getTableBusOperator();
                JSONObject object = array.getJSONObject(0);
                query = query.concat(" SELECT '").concat(object.getString("id")).concat("' AS 'IdBusOperator', '").concat(object.getString("official_name")).concat("' AS 'NameBusOperator', '").concat(object.getString("icon_url")).concat("' AS 'icon_url' ");
                for (int j = 1; j < total; j++) {
                    object = array.getJSONObject(j);
                    query = query.concat(" UNION SELECT '").concat(object.getString("id")).concat("', '").concat(object.getString("official_name")).concat("', '").concat(object.getString("icon_url")).concat("'");
                }
                Log.v("query", query);
                db.Execute(query);
            }


        } catch (NullPointerException e) {
            e.printStackTrace();
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    //    FillBusOperator
    public void DestinyProcess(JSONArray objeto, String Origen) {
        try {
            db.DestinyDeleteAll(Origen);
            int totalj = objeto.length();
            if (totalj > 0) {
                String query = "INSERT INTO " + db.getTableDestiny();
//                JSONObject objetob = objeto.getJSONObject(0);
                query = query.concat(" SELECT '").concat(Origen).concat("' AS 'departure_idDeparture', '").concat(objeto.get(0).toString()).concat("' AS 'destiny_idDestiny'");
                for (int j = 1; j < totalj; j++) {
//                    objetob = objeto.getJSONObject(j);
                    query = query.concat(" UNION SELECT '").concat(Origen).concat("', '").concat(objeto.get(j).toString()).concat("'");
                }
                Log.v("query", query);
                db.Execute(query);


            }

        } catch (NullPointerException e) {
            e.printStackTrace();
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    public ObjectState BusTravels(Context contexto, String base, String DepartureCityID, String DestinationCityID, String DepartureDate, String ReturnDate, boolean isRoundTrip) throws ClientProtocolException, IOException {
        ObjectState es = new ObjectState();
        try {
            Map<String, String> kvPairs = new HashMap<String, String>();
            kvPairs.put("bus_travel[destination_city_id]", DestinationCityID);
            kvPairs.put("bus_travel[departure_city_id]", DepartureCityID);
            kvPairs.put("bus_travel[departure_date]", DepartureDate);
            if (isRoundTrip) {
                kvPairs.put("bus_travel[return_date]", ReturnDate);
            }
            db = new DB(contexto);
            HttpClient httpclient = this.getNewHttpClient();
            Log.v("url", "BusTravels");
            HttpPost httppost = new HttpPost(base + "en/bus_travels.json");
            if (kvPairs != null && kvPairs.isEmpty() == false) {
                List<NameValuePair> nameValuePairs = new ArrayList<NameValuePair>(kvPairs.size());
                String k, v;
                Iterator<String> itKeys = kvPairs.keySet().iterator();

                while (itKeys.hasNext()) {
                    k = itKeys.next();
                    v = kvPairs.get(k);
                    nameValuePairs.add(new BasicNameValuePair(k, v));
                }

                httppost.setEntity(new UrlEncodedFormEntity(nameValuePairs, "UTF-8"));
            }

            HttpResponse response;

            response = httpclient.execute(httppost);
            String respuesta = EntityUtils.toString(response.getEntity());
            Log.v("response", respuesta);
            try {
                if (response.getStatusLine().getStatusCode() == 200) {
                    if (respuesta != null) {
                        JSONObject jsonObj = new JSONObject(respuesta);
                        int progress = Integer.parseInt(jsonObj.getString("bus_travel_url").toString().replace("/directions/outbound.json", "").replace("/api/v1/es/bus_travels/", "").replace("/api/v1/en/bus_travels/", ""));
                        es = this.BusTravelsGet(contexto, base, progress);
//                        if (jsonObj.getBoolean("round_trip")) {
//                        es = this.BusTravelsGet(contexto, base, progress, "inbound");
//                        }
                        es.setNumberJustInCase(progress);
                        es.setKeepWaiting(false);

                        SharedPreferences settings = contexto.getSharedPreferences("cl.recorrido.micritodroid", Context.MODE_PRIVATE);
//                        Integer TravelID=Integer.parseInt(settings.getString("SearchID",""));
//                        if(TravelID!=null) {
//                            settings = contexto.getSharedPreferences("cl.recorrido.micritodroid", Context.MODE_PRIVATE);
                        SharedPreferences.Editor editor = settings.edit();
                        editor.putString("SearchID", String.valueOf(progress));
                        editor.putString("RoundTrip", String.valueOf(es.getAnotherNumberJustInCase()));
                        editor.putString("TravelType", "outbound");
                        editor.commit();
//                        }
                    }
                } else if (response.getStatusLine().getStatusCode() == 201) {
                    if (respuesta != null) {
                        JSONObject jsonObj = new JSONObject(respuesta);
                        es.setNumberJustInCase(jsonObj.getJSONObject("bus_travel").getInt("id"));
                        es.setKeepWaiting(true);
                        es.setState(true);
                        es.setProgress(0);
                    }
                } else {
                    es.setState(false);
                    es.setCode(String.valueOf(response.getStatusLine().getStatusCode()));
                }
            } catch (JSONException e) {
                Log.v("JSONException", "");
                e.getMessage();
                e.printStackTrace();
            }
        } catch (IOException e) {
            Log.v("IOEXception", "");
            e.getMessage();
            e.printStackTrace();
        }


        return es;
    }

    public ObjectState BusTravelsGet(Context contexto, String base, int ProgressId) throws ClientProtocolException, IOException {
        ObjectState es = new ObjectState();
        Util u = new Util();
        try {
            db = new DB(contexto);
            HttpClient httpclient = this.getNewHttpClient();
            Log.v("url", "BusTravelsGet");
//            https://50.116.39.28/api/v1/es/bus_travels/14918/directions/outbound.json
            HttpGet httppost = new HttpGet(base + "en/bus_travels/" + String.valueOf(ProgressId) + ".json");
            HttpResponse response;
            response = httpclient.execute(httppost);
            String respuesta = EntityUtils.toString(response.getEntity());
            Log.v("response", respuesta);
            try {
                if (response.getStatusLine().getStatusCode() == 200) {
                    db.TravelDelete(ProgressId);
                    db.ScheduleDeleteAll(ProgressId);

                    if (respuesta != null) {
                        JSONObject jsonObj = new JSONObject(respuesta);
                        JSONObject bus_travel = jsonObj.getJSONObject("bus_travel");
                        ObjectBusTravel obt = new ObjectBusTravel();
                        obt.setId(bus_travel.getInt("id"));
                        obt.setDepartureDate(u.StringToDate(bus_travel.getString("departure_date"), "\\.", "d/m/Y"));
                        if (bus_travel.getString("return_date") == null) {
                            obt.setReturnDate(u.StringToDate("0/0/0001", ".", "d/m/Y"));
                        } else {
                            obt.setReturnDate(u.StringToDate(bus_travel.getString("departure_date"), "\\.", "d/m/Y"));
                        }
                        obt.setAggregator(0);
                        ObjectCity DestinationCity = new ObjectCity(bus_travel.getInt("destination_city_id"));
                        ObjectCity DepartureCity = new ObjectCity(bus_travel.getInt("departure_city_id"));
//                        ObjectCity DepartureCity = new ObjectCity(bus_travel.getJSONObject("departure_city").getInt("id"), bus_travel.getJSONObject("departure_city").getString("name"));
                        obt.setDepartureCity(DepartureCity);
                        obt.setDestinationCity(DestinationCity);
                        obt.setRoundTrip(bus_travel.getBoolean("round_trip"));
                        db.TravelAdd(obt);
                        //outbound_bus_schedules
                        JSONArray outbound_bus_schedules = bus_travel.getJSONArray("outbound_bus_schedules");
                        ProcessSchedules(outbound_bus_schedules, ProgressId, 1);
                        JSONArray inbound_bus_schedules = bus_travel.getJSONArray("inbound_bus_schedules");
                        ProcessSchedules(inbound_bus_schedules, ProgressId, 2);
                        es.setState(true);
                        es.setAnotherNumberJustInCase(u.BoolToInt(obt.isRoundTrip()));
                    }
                } else {
                    es.setState(false);
                    es.setCode(String.valueOf(response.getStatusLine().getStatusCode()));
                }
            } catch (JSONException e) {
                Log.v("JSONException", "");
                e.getMessage();
                e.printStackTrace();
            }
        } catch (IOException e) {
            Log.v("IOEXception", "");
            e.getMessage();
            e.printStackTrace();
        }


        return es;
    }

//    JSONArray outbound_bus_schedules = bus_travel.getJSONArray("outbound_bus_schedules");
//    int TotalSchedules = outbound_bus_schedules.length();

    public void ProcessSchedules(JSONArray outbound_bus_schedules, int ProgressId, int type) {
        Util u = new Util();
        int TotalSchedules = outbound_bus_schedules.length();
        try {
            for (int i = 0; i < TotalSchedules; i++) {
                JSONObject JSONObs = outbound_bus_schedules.getJSONObject(i);

                ObjectBusSchedule BusSchedule = new ObjectBusSchedule();
                BusSchedule.setId(JSONObs.getInt("id"));
                BusSchedule.setTravelTimeMinutes(u.JsonToInt(JSONObs, "travel_time_in_minutes"));
                if (!JSONObs.isNull("departs_at"))
                    BusSchedule.setDeparture(u.StringToDate(JSONObs.getString("departs_at").replace(".000-03:00", "").replace("T", " "), "-", "d/M/y H:m:s"));
                else
                    BusSchedule.setDeparture(u.StringToDate("0000-00-00 00:00:00", "-", "d/M/y H:m:s"));
                if (!JSONObs.isNull("arrives_at"))
                    BusSchedule.setArrives(u.StringToDate(JSONObs.getString("arrives_at").replace(".000-03:00", "").replace("T", " "), "-", "d/M/y H:m:s"));
                else
                    BusSchedule.setArrives(u.StringToDate("0000-00-00 00:00:00", "-", "d/M/y H:m:s"));
                ObjectBusOperator buo = new ObjectBusOperator();
                buo.setIdBusOperator(JSONObs.getInt("bus_operator_id"));
                BusSchedule.setBusOperator(buo);


                BusSchedule.setTerminalDeparture(JSONObs.getString("terminal_departure"));
                BusSchedule.setTerminalArrival(JSONObs.getString("terminal_arrival"));
                BusSchedule.setSeat_klass(JSONObs.getString("seat_klass"));

//                            BusSchedule.setPrice(JSONObs.getInt("price"));
                BusSchedule.setPrice(u.JsonToInt(JSONObs, "price"));
                BusSchedule.setResourceUrl(JSONObs.getString("resource_url"));

                BusSchedule.setSeatClassStars(u.JsonToInt(JSONObs, "seat_klass_stars"));
                BusSchedule.setType(type);

                db.ScheduleAdd(BusSchedule, ProgressId);
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    public ObjectState BusTravelsProgress(Context contexto, String base, int ProgressId) throws ClientProtocolException, IOException {
        ObjectState es = new ObjectState();
        try {
            Util u = new Util();
            db = new DB(contexto);
            HttpClient httpclient = this.getNewHttpClient();
            Log.v("url", "BusTravelsProgress");
            HttpGet httppost = new HttpGet(base + "en/search_progresses/" + String.valueOf(ProgressId));
            HttpResponse response;
            response = httpclient.execute(httppost);
            String respuesta = EntityUtils.toString(response.getEntity());
            Log.v("response", respuesta);
            try {
                if (response.getStatusLine().getStatusCode() == 200 || response.getStatusLine().getStatusCode() == 201) {
                    if (respuesta != null) {
                        JSONObject jsonObj = new JSONObject(respuesta);
                        if (jsonObj.getString("progress_status").compareTo("complete") == 0) {
//                        es.setState(true);
                            es.setKeepWaiting(false);
                            es.setState(true);
                            //get travel
                            es = this.BusTravelsGet(contexto, base, ProgressId);
                            SharedPreferences settings = contexto.getSharedPreferences("cl.recorrido.micritodroid", Context.MODE_PRIVATE);
                            SharedPreferences.Editor editor = settings.edit();
                            editor.putString("SearchID", String.valueOf(ProgressId));
                            editor.putString("RoundTrip", String.valueOf(es.getAnotherNumberJustInCase()));
                            editor.putString("TravelType", "outbound");
                            editor.commit();


                        } else {
                            es.setProgress(jsonObj.getInt("progress_status"));
                            es.setKeepWaiting(true);
                            es.setState(true);
                        }
                    }
                } else {
                    es.setState(false);
                    es.setCode(String.valueOf(response.getStatusLine().getStatusCode()));
                }
                Log.v("status code", String.valueOf(response.getStatusLine().getStatusCode()));
//                Log.v("status code", String.valueOf(response.getStatusLine().getStatusCode()));
            } catch (JSONException e) {
                Log.v("JSONException", "");
                e.getMessage();
                e.printStackTrace();
            }
        } catch (IOException e) {
            Log.v("IOEXception", "");
            e.getMessage();
            e.printStackTrace();
        }
        finally {
            db.close();
        }


        return es;
    }

    public void ProcessOutbound(JSONObject jsonObj) {
//        TravelAdd

    }

    public ObjectState Outbound(Context contexto, String base, String URL) throws ClientProtocolException, IOException {
        ObjectState es = new ObjectState();
        try {
            db = new DB(contexto);
            HttpClient httpclient = this.getNewHttpClient();
            HttpGet httppost = new HttpGet(base + URL.replace("/api/v1/", "") + "per_page=1000");
            HttpResponse response;

            response = httpclient.execute(httppost);
            String respuesta = EntityUtils.toString(response.getEntity());
            Log.v("response", respuesta);
            try {
                if (response.getStatusLine().getStatusCode() == 200) {
                    if (respuesta != null) {
                        JSONObject jsonObj = new JSONObject(respuesta);
                        this.ProcessOutbound(jsonObj);
                        es.setKeepWaiting(false);
                        es.setState(true);
                    }
                } else if (response.getStatusLine().getStatusCode() == 404) {
                    if (respuesta != null) {
                        es.setState(false);
                        es.setCode("404");
                    }
                } else {
                    es.setState(false);
                    es.setCode(String.valueOf(response.getStatusLine().getStatusCode()));
                }
            } catch (JSONException e) {
                Log.v("JSONException", "");
                //progressBar.setVisibility(View.INVISIBLE);
                e.getMessage();
                e.printStackTrace();
            }
        } catch (IOException e) {
            //progressBar.setVisibility(View.INVISIBLE);
            Log.v("IOEXception", "");
            e.getMessage();
            e.printStackTrace();
        }
        finally {
            db.close();
        }


        return es;
    }
}
