package cl.recorrido.micritodroid;

import android.content.Intent;
import android.os.AsyncTask;
import android.os.StrictMode;
import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.view.Window;
import android.widget.GridLayout;


public class ActivityLoading extends ActionBarActivity {
boolean sigue = false;
    @Override
    protected void onCreate(Bundle savedInstanceState) {

        this.supportRequestWindowFeature(Window.FEATURE_NO_TITLE);
//        getSupportActionBar().hide();
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        getWindow().setLayout(GridLayout.LayoutParams.MATCH_PARENT, GridLayout.LayoutParams.MATCH_PARENT);

//        this.getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_loading);
//        new Handler().postDelayed(new Runnable(){
//            @Override
//            public void run() {
//                Intent SearchIntent = new Intent(ActivityLoading.this, ActivityMain.class);
//                ActivityLoading.this.startActivity(SearchIntent);
//                ActivityLoading.this.finish();
//            }
//        }, 1000);
        DB db = new DB(getBaseContext());
        boolean sigue = true;
        if(db.TotalCities()==0){
            sigue=false;
        }else if(db.TotalCountries()==0){
            sigue=false;
        }else if(db.TotalBusOperator()==0){
            sigue=false;
        }
        if(!sigue) {
            new AsyncTask<Void, Void, Void>() {
                @Override
                protected Void doInBackground(Void... params) {
                    new Thread(new Runnable() {
                        public void run() {
                            try {
                                StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
                                StrictMode.setThreadPolicy(policy);
                                Connection con = new Connection();

                                ObjectState respuesta = con.FirstLoad(getBaseContext(), getBaseContext().getString(R.string.url_base));
                                if (respuesta.isState()) {
                                    Intent myIntent = new Intent(ActivityLoading.this, ActivityMain.class);
                                    startActivity(myIntent);

                                } else {
                                    Intent myIntent = new Intent(ActivityLoading.this, ActivityMain.class);
                                    startActivity(myIntent);
                                }
                            } catch (Exception e) {

                                e.printStackTrace();
                            }
                        }
                    }).start();

                    return null;
                }

                @Override
                protected void onPostExecute(Void result) {

                    Intent myIntent = new Intent(ActivityLoading.this, ActivityMain.class);
                    startActivity(myIntent);
                }
            }.execute();


        }else{

            Intent myIntent = new Intent(ActivityLoading.this, ActivityMain.class);
            startActivity(myIntent);
        }
    }
}
