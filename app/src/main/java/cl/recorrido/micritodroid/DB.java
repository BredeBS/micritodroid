package cl.recorrido.micritodroid;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteException;
import android.database.sqlite.SQLiteOpenHelper;
import android.database.sqlite.SQLiteStatement;
import android.util.Log;

import java.util.ArrayList;

/**
 * Created by Brede on 03-11-14.
 */
public class DB extends SQLiteOpenHelper {
    Util u;
    String TableCities = "cities";
    String TableCountry = "country";
    String TableDestiny = "destination";
    String Travel = "travel";
    String Schedules = "schedules";
    String TableBusOperator = "busoperator";


    public String getTableCities() {
        return TableCities;
    }

    public String getTableDestiny() {
        return TableDestiny;
    }

    public String getTravel() {
        return Travel;
    }

    public String getSchedules() {
        return Schedules;
    }

    public String getTableCountry() {
        return TableCountry;
    }


    String createTableComunas = "CREATE TABLE if not exists " + TableCities + " (id INTEGER, name TEXT,city_country_id INTEGER)";
    String createTableBusOperator = "CREATE TABLE if not exists " + TableBusOperator + " (IdBusOperator INTEGER, NameBusOperator TEXT,BusOperatorIconUrl TEXT)";
    String createTableCountry = "CREATE TABLE if not exists " + TableCountry + " (country_id INTEGER, country_name TEXT)";
    String createTabledestiny_idDestiny = "CREATE TABLE if not exists " + TableDestiny + " (departure_idDeparture INTEGER, destiny_idDestiny INTEGER)";
    String createTableTravel = "CREATE TABLE if not exists " + Travel + " (travel_id INTEGER, departure_date DATETIME, return_date DATETIME, round_trip INTEGER, aggregator_id INTEGER, destination_city_id INTEGER, departure_city_id INTEGER)";
    String createTableSchedules = "CREATE TABLE if not exists " + Schedules + " (travel_id INTEGER,schedules_id INTEGER, bus_operator_id, departs_at DATETIME, arrives_at DATETIME, price INTEGER, seat_klass_stars INTEGER, resource_url TEXT,travel_time_minutes INTEGER, type INTEGER, terminal_departure TEXT, terminal_arrival TEXT, seat_klass TEXT)";
//    String createTableSchedulesTravel = "CREATE TABLE if not exists " + SchedulesTravel + " (st_schedules_id INTEGER,st_travel_id INTEGER)";

    public DB(Context context) {
        super(context, "recorrido", null, 13);
        this.onCreate(this.getWritableDatabase());
    }


    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL(createTableCountry);
        db.execSQL(createTableComunas);
        db.execSQL(createTabledestiny_idDestiny);
        db.execSQL(createTableTravel);
        db.execSQL(createTableSchedules);
        db.execSQL(createTableBusOperator);
//        db.execSQL(createTableSchedulesTravel);
    }


    @Override
    public void onUpgrade(SQLiteDatabase sqLiteDatabase, int i, int i2) {
        DropAllDatabase(sqLiteDatabase);
        onCreate(sqLiteDatabase);
    }

    public void DropAllDatabase(SQLiteDatabase sqLiteDatabase) {
        sqLiteDatabase.execSQL("DROP TABLE IF EXISTS " + TableCities);
        sqLiteDatabase.execSQL("DROP TABLE IF EXISTS " + TableDestiny);
        sqLiteDatabase.execSQL("DROP TABLE IF EXISTS " + TableCountry);
        sqLiteDatabase.execSQL("DROP TABLE IF EXISTS " + TableCities);
        sqLiteDatabase.execSQL("DROP TABLE IF EXISTS " + Travel);
        sqLiteDatabase.execSQL("DROP TABLE IF EXISTS " + Schedules);
        sqLiteDatabase.execSQL("DROP TABLE IF EXISTS " + TableBusOperator);
//        sqLiteDatabase.execSQL("DROP TABLE IF EXISTS " + SchedulesTravel);
    }

    public void Salir() {
        SQLiteDatabase db = this.getWritableDatabase();
        DropAllDatabase(db);
        db.close();
    }

    public String getTableBusOperator() {
        return TableBusOperator;
    }

    public void Execute(String consulta) {
        // Adding new contact
        SQLiteDatabase db = this.getWritableDatabase();
        onCreate(db);
        db.execSQL(consulta);

    }

    public void CityAdd(ObjectCity p) {
        SQLiteDatabase db = this.getWritableDatabase();
        onCreate(db);
        ContentValues values = new ContentValues();
        values.put("id", p.getId()); // City id
        values.put("name", p.getName());
        db.insert(TableCities, null, values);
        db.close(); // Closing database connection

    }

    public void AddBusOperator(ObjectBusOperator p) {
        SQLiteDatabase db = this.getWritableDatabase();
        onCreate(db);
        ContentValues values = new ContentValues();
        values.put("id", p.getIdBusOperator()); // City id
        values.put("name", p.getNameBusOperator());
        values.put("BusOperatorIconUrl", p.getIcon_url());
        db.insert(TableBusOperator, null, values);
        db.close(); // Closing database connection

    }

    public void TravelAdd(ObjectBusTravel p) {
        SQLiteDatabase db = this.getWritableDatabase();
        onCreate(db);
        u = new Util();
        ContentValues values = new ContentValues();
        values.put("travel_id", p.getId()); // City id
        values.put("departure_date", u.DateTimeToString(p.getDepartureDate(), "dd/MM/yyyy"));

        values.put("return_date", u.DateTimeToString(p.getReturnDate(), "dd/MM/yyyy"));
        values.put("round_trip", u.BoolToInt(p.isRoundTrip()));
        values.put("aggregator_id", p.getAggregator());
        values.put("destination_city_id", p.getDestinationCity().getId());
        values.put("departure_city_id", p.getDepartureCity().getId());
        db.insert(Travel, null, values);
        db.close(); // Closing database connection

    }

    public void ScheduleAdd(ObjectBusSchedule p, int ProgressId) {
        SQLiteDatabase db = this.getWritableDatabase();
        onCreate(db);
        u = new Util();
        ContentValues values = new ContentValues();
        values.put("schedules_id", p.getId()); // City id
        values.put("travel_id", ProgressId); // City id
        values.put("bus_operator_id", p.getBusOperator().getIdBusOperator());
        values.put("departs_at", u.DateTimeToString(p.getDeparture(), "dd/MM/yyyy HH:mm:ss"));
        values.put("arrives_at", u.DateTimeToString(p.getArrives(), "dd/MM/yyyy HH:mm:ss"));
        values.put("price", p.getPrice());
        values.put("seat_klass_stars", p.getSeatClassStars());
        values.put("resource_url", p.getResourceUrl());
        values.put("travel_time_minutes", p.getTravelTimeMinutes());
        values.put("terminal_departure", p.getTerminalDeparture());
        values.put("terminal_arrival", p.getTerminalArrival());
        values.put("seat_klass", p.getSeat_klass());
        values.put("type", p.getType());


//        terminal_departure TEXT, terminal_arrival TEXT, seat_klass TEXT

        db.insert(Schedules, null, values);
//        ContentValues valuesST = new ContentValues();
//        valuesST.put("st_schedules_id", p.getId()); // City id
//        valuesST.put("st_travel_id", ProgressId);
//        db.insert(SchedulesTravel, null, valuesST);
        db.close(); // Closing database connection
    }

    public void CountryAdd(ObjectCountry p) {
        SQLiteDatabase db = this.getWritableDatabase();
        onCreate(db);
        ContentValues values = new ContentValues();
        values.put("country_id", p.getCountryId()); // City id
        values.put("country_name", p.getCountryName());
        db.insert(TableCountry, null, values);
        db.close(); // Closing database connection

    }

    public void DeleteAllFromTable(String table) {
        SQLiteDatabase db = this.getWritableDatabase();
        db.execSQL("Delete from " + table);
        db.close();
    }

    public void CityDeleteAll() {
        SQLiteDatabase db = this.getWritableDatabase();
        db.execSQL("Delete from " + TableCities);
        db.close();
    }

    public void DestinyDeleteAll(String departure_idDeparture) {
        SQLiteDatabase db = this.getWritableDatabase();
        db.execSQL("Delete from " + TableDestiny + " where departure_idDeparture=" + departure_idDeparture);
        db.close();
    }

    public void TravelDelete(int departure_idDeparture) {
        SQLiteDatabase db = this.getWritableDatabase();
        db.execSQL("Delete from " + Travel + " where travel_id=" + String.valueOf(departure_idDeparture));
        db.close();
    }

    public void ScheduleDeleteAll(int departure_idDeparture) {
        SQLiteDatabase db = this.getWritableDatabase();

        // We delete all schedules to avoid overwriting it
        db.execSQL("Delete from " + Schedules + " where travel_id=" + String.valueOf(departure_idDeparture));
        db.close();
    }

    public void CityDelete(String id) {
        SQLiteDatabase db = this.getWritableDatabase();
        db.execSQL("Delete from " + TableCities + " where id='" + id + "'");
        db.close();
    }


    public ArrayList<ObjectCity> CityListAll(int CountryId) {
        ArrayList<ObjectCity> collection = new ArrayList<ObjectCity>();
        Cursor cursor;
        SQLiteDatabase db = this.getReadableDatabase();
        String MY_QUERY = "select id,name from " + TableCities + " where city_country_id=" + String.valueOf(CountryId) + " order by name asc";
        cursor = db.rawQuery(MY_QUERY, null);
        try {
            if (cursor != null && cursor.getCount() > 0) {
                while (cursor.moveToNext()) {
                    ObjectCity objCity = new ObjectCity(cursor.getInt(0), cursor.getString(1));
                    collection.add(objCity);
                }
            }
        } catch (SQLiteException ex) {
            Log.v("Exception ", ex.getMessage());
        } finally {
            cursor.close();
            db.close();
        }
        return collection;
    }

    public ArrayList<ObjectCity> DestinyListAll(int CountryID, String OriginCityID) {
        ArrayList<ObjectCity> collection = new ArrayList<ObjectCity>();
        Cursor cursor;
        SQLiteDatabase db = this.getReadableDatabase();
//        String[] campos = new String[]{"id", "name"};
//        cursor = db.query(TableCities, campos, null, null, null, null, null);
        String MY_QUERY = "select id,name from " + TableCities + " JOIN " + TableDestiny + " on destiny_idDestiny=id where city_country_id=" + CountryID + " and departure_idDeparture=" + String.valueOf(OriginCityID) + "  order by name asc  ";

        cursor = db.rawQuery(MY_QUERY, null);
        try {
            if (cursor != null && cursor.getCount() > 0) {
                while (cursor.moveToNext()) {
                    ObjectCity objCity = new ObjectCity(cursor.getInt(0), cursor.getString(1));
                    collection.add(objCity);
                }
            }
        } catch (SQLiteException ex) {
            Log.v("Exception ", ex.getMessage());
        } finally {
            cursor.close();
            db.close();
        }
        return collection;
    }

    public ArrayList<ObjectCountry> CountryGetAll() {
        ArrayList<ObjectCountry> collection = new ArrayList<ObjectCountry>();
        Cursor cursor;
        SQLiteDatabase db = this.getReadableDatabase();
//        String[] campos = new String[]{"id", "name"};
//        cursor = db.query(TableCities, campos, null, null, null, null, null);
        String MY_QUERY = "select country_id,country_name from " + TableCountry;

        cursor = db.rawQuery(MY_QUERY, null);
        try {
            if (cursor != null && cursor.getCount() > 0) {
                while (cursor.moveToNext()) {
                    ObjectCountry objCity = new ObjectCountry(cursor.getInt(0), cursor.getString(1));
                    collection.add(objCity);
                }
            }
        } catch (SQLiteException ex) {
            Log.v("Exception ", ex.getMessage());
        } finally {
            cursor.close();
            db.close();
        }
        return collection;
    }

    public ArrayList<ObjectCity> CityListFilter(int CountryID, String Filter) {
        ArrayList<ObjectCity> collection = new ArrayList<ObjectCity>();
        Cursor cursor;
        SQLiteDatabase db = this.getReadableDatabase();
        String[] campos = new String[]{"id", "name"};

        String MY_QUERY = "select id,name from " + TableCities + " where name LIKE '%" + Filter + "%' and city_country_id=" + String.valueOf(CountryID) + " order by name asc";
        cursor = db.rawQuery(MY_QUERY, null);
        try {
            if (cursor != null && cursor.getCount() > 0) {
                while (cursor.moveToNext()) {
                    ObjectCity objCity = new ObjectCity(cursor.getInt(0), cursor.getString(1));
                    collection.add(objCity);
                }
            }
        } catch (SQLiteException ex) {
            Log.v("Exception ", ex.getMessage());
        } finally {
            cursor.close();
            db.close();
        }
        return collection;
    }

    public ArrayList<ObjectCity> DestinyFilter(int DestinationCountryID, String OriginCityID, String key) {
        ArrayList<ObjectCity> collection = new ArrayList<ObjectCity>();
        Cursor cursor;
        SQLiteDatabase db = this.getReadableDatabase();
        String MY_QUERY = "select id,name from " + TableCities + " JOIN " + TableDestiny + " on destiny_idDestiny=id where departure_idDeparture=" + String.valueOf(OriginCityID) + " and city_country_id=" + String.valueOf(DestinationCountryID) + " and name like'%" + key + "%' order by name asc";

        cursor = db.rawQuery(MY_QUERY, null);
        try {
            if (cursor != null && cursor.getCount() > 0) {
                while (cursor.moveToNext()) {
                    ObjectCity objCity = new ObjectCity(cursor.getInt(0), cursor.getString(1));
                    collection.add(objCity);
                }
            }
        } catch (SQLiteException ex) {
            Log.v("Exception ", ex.getMessage());
        } finally {
            cursor.close();
            db.close();
        }
        return collection;
    }

    public ArrayList<ObjectBusTravel> getTravel(int ScheduleID, String Direction, int Rating, int Departure, int Fare) {
        ArrayList<ObjectBusTravel> collection = new ArrayList<ObjectBusTravel>();
        Cursor cursor;

        u = new Util();
        SQLiteDatabase db = this.getReadableDatabase();
        String MY_QUERY = "select departure_date, return_date, round_trip, destination_city_id, departure_city_id from " + Travel + " where travel_id =" + String.valueOf(ScheduleID);
        cursor = db.rawQuery(MY_QUERY, null);
        try {
            if (cursor != null && cursor.getCount() > 0) {
                while (cursor.moveToNext()) {
                    ObjectBusTravel travel = new ObjectBusTravel();
                    travel.setId(ScheduleID);
                    travel.setDepartureDate(u.StringToDate(cursor.getString(0), "/", "d/m/Y"));
                    travel.setReturnDate(u.StringToDate(cursor.getString(1), "/", "d/m/Y"));
                    travel.setRoundTrip(u.IntToBool(cursor.getInt(2)));
                    travel.setDestinationCity(this.getCityById(cursor.getInt(3)));
                    travel.setDepartureCity(this.getCityById(cursor.getInt(4)));
                    travel.setBusOperator(this.getBusOperatorSchedules(ScheduleID, Direction, Rating, Departure, Fare));
                    collection.add(travel);
                }
            }
        } catch (SQLiteException ex) {
            Log.v("Exception ", ex.getMessage());
        } finally {
            cursor.close();
            db.close();
        }
        return collection;
    }

    public ObjectBusTravel getOperatorTravel(int ScheduleID, String Direction, int Rating, int Departure, int Fare) {
        Cursor cursor;
        ObjectBusTravel travel = new ObjectBusTravel();
        u = new Util();
        SQLiteDatabase db = this.getReadableDatabase();
        String MY_QUERY = "select departure_date, return_date, round_trip, destination_city_id, departure_city_id from " + Travel + " where travel_id =" + String.valueOf(ScheduleID);
        cursor = db.rawQuery(MY_QUERY, null);
        try {
            if (cursor != null && cursor.getCount() > 0) {
                while (cursor.moveToNext()) {

                    travel.setId(ScheduleID);
                    travel.setDepartureDate(u.StringToDate(cursor.getString(0), "/", "d/m/Y"));
                    travel.setReturnDate(u.StringToDate(cursor.getString(1), "/", "d/m/Y"));
                    travel.setRoundTrip(u.IntToBool(cursor.getInt(2)));
                    travel.setDestinationCity(this.getCityById(cursor.getInt(3)));
                    travel.setDepartureCity(this.getCityById(cursor.getInt(4)));
                    travel.setBusOperator(this.getBusOperatorSchedules(ScheduleID, Direction, Rating, Departure, Fare));

                }
            }
        } catch (SQLiteException ex) {
            Log.v("Exception ", ex.getMessage());
        } finally {
            cursor.close();
            db.close();
        }
        return travel;
    }

    public int TotalSchedules(int ScheduleID, String Direction) {
        int total = 0;
//    simpleQueryForLong
        String type = "1";
        if (Direction.compareTo("inbound") == 0) {
            type = "2";
        }
        Util u = new Util();
        SQLiteDatabase db = this.getReadableDatabase();
        SQLiteStatement s = db.compileStatement("select count(*) from " + Schedules + " where travel_id=" + String.valueOf(ScheduleID) + " and type=" + type);
        long count = s.simpleQueryForLong();
        total = u.safeLongToInt(count);
        return total;
    }

    public int TotalCities() {
        int total = 0;
        Util u = new Util();
        SQLiteDatabase db = this.getReadableDatabase();
        SQLiteStatement s = db.compileStatement("select count(*) from " + TableCities + " ");
        long count = s.simpleQueryForLong();
        total = u.safeLongToInt(count);
        return total;
    }

    public int TotalCountries() {
        int total = 0;
        Util u = new Util();
        SQLiteDatabase db = this.getReadableDatabase();
        SQLiteStatement s = db.compileStatement("select count(*) from " + TableCountry + " ");
        long count = s.simpleQueryForLong();
        total = u.safeLongToInt(count);
        return total;
    }

    public int TotalBusOperator() {
        int total = 0;
        Util u = new Util();
        SQLiteDatabase db = this.getReadableDatabase();
        SQLiteStatement s = db.compileStatement("select count(*) from " + TableBusOperator + " ");
        long count = s.simpleQueryForLong();
        total = u.safeLongToInt(count);
        return total;
    }

    public ArrayList<ObjectBusSchedule> getSchedules(int ScheduleID, String Direction, int Rating, int Departure, int Fare) {
        String extra = "";
        String extratmp = "asc";
        if (Rating == 1 || Rating == 2) {
            extra = " order by seat_klass_stars ";
            if (Rating == 2)
                extratmp = "desc";
        } else if (Departure == 1 || Departure == 2) {
            extra = " order by departs_at ";
            if (Departure == 2)
                extratmp = "desc";
        } else if (Fare == 1 || Fare == 2) {
            extra = " order by price ";
            if (Fare == 2)
                extratmp = "desc";
        }
        if (extra.compareTo("") != 0) {
            extra = extra.concat(" ").concat(extratmp);
        }
        ArrayList<ObjectBusSchedule> collection = new ArrayList<ObjectBusSchedule>();
        Cursor cursor;
        u = new Util();
        SQLiteDatabase db = this.getReadableDatabase();
        String type = "1";
        if (Direction.compareTo("inbound") == 0) {
            type = "2";
        }
//        (travel_id INTEGER,schedules_id INTEGER, bus_operator_id, departs_at DATETIME, arrives_at DATETIME, price INTEGER, seat_klass_stars INTEGER, resource_url TEXT,travel_time_minutes INTEGER, type INTEGER, terminal_departure TEXT, terminal_arrival TEXT, seat_klass TEXT)
        String MY_QUERY = "select schedules_id, bus_operator_id, departs_at, arrives_at, price,seat_klass_stars,resource_url,travel_time_minutes,terminal_departure , terminal_arrival , seat_klass from " + Schedules + " where travel_id =" + String.valueOf(ScheduleID)
                + " and type=" + type + extra;
        cursor = db.rawQuery(MY_QUERY, null);
        try {
            if (cursor != null && cursor.getCount() > 0) {
                while (cursor.moveToNext()) {
                    ObjectBusSchedule travel = new ObjectBusSchedule();
                    travel.setId(cursor.getInt(0));
                    travel.setBusOperator(getBusOperatorById(cursor.getInt(1)));
                    travel.setDeparture(u.StringToDate(cursor.getString(2), "/", "d/M/y H:m:s"));
                    travel.setArrives(u.StringToDate(cursor.getString(3), "/", "d/M/y H:m:s"));
                    travel.setPrice(cursor.getInt(4));
                    travel.setSeatClassStars(cursor.getInt(5));
                    travel.setResourceUrl(cursor.getString(6));
                    travel.setTravelTimeMinutes(cursor.getInt(7));
                    travel.setTerminalDeparture(cursor.getString(8));
                    travel.setTerminalArrival(cursor.getString(9));
                    travel.setSeat_klass(cursor.getString(10));
                    collection.add(travel);
                }
            }
        } catch (SQLiteException ex) {
            Log.v("Exception ", ex.getMessage());
        } finally {
            cursor.close();
            db.close();
        }
        return collection;
    }

    public ArrayList<ObjectBusOperatorSchedule> getBusOperatorSchedules(int ScheduleID, String Direction, int Rating, int Departure, int Fare) {
        String extra = "";
        String extratmp = "asc";
        if (Rating == 1 || Rating == 2) {
            extra = " order by seat_klass_stars ";
            if (Rating == 2)
                extratmp = "desc";
        } else if (Departure == 1 || Departure == 2) {
            extra = " order by departs_at ";
            if (Departure == 2)
                extratmp = "desc";
        } else if (Fare == 1 || Fare == 2) {
            extra = " order by price ";
            if (Fare == 2)
                extratmp = "desc";
        }
        if (extra.compareTo("") != 0) {
            extra = extra.concat(" ").concat(extratmp);
        }
        ArrayList<ObjectBusOperatorSchedule> collection = new ArrayList<ObjectBusOperatorSchedule>();
        Cursor cursorOperators, cursorTravels;
        u = new Util();
        SQLiteDatabase dbOperators = this.getReadableDatabase();
        String type = "1";
        if (Direction.compareTo("inbound") == 0) {
            type = "2";
        }
//        (travel_id INTEGER,schedules_id INTEGER, bus_operator_id, departs_at DATETIME, arrives_at DATETIME, price INTEGER, seat_klass_stars INTEGER, resource_url TEXT,travel_time_minutes INTEGER, type INTEGER, terminal_departure TEXT, terminal_arrival TEXT, seat_klass TEXT)
        String QueryOperators = "select distinct(bus_operator_id) from " + Schedules + " where travel_id =" + String.valueOf(ScheduleID)
                + " and type=" + type + extra;
        cursorOperators = dbOperators.rawQuery(QueryOperators, null);

        try {
            if (cursorOperators != null && cursorOperators.getCount() > 0) {
                while (cursorOperators.moveToNext()) {
                    SQLiteDatabase dbTravels = this.getReadableDatabase();
//, schedules_id, bus_operator_id, departs_at, arrives_at, price,seat_klass_stars,resource_url,travel_time_minutes,terminal_departure , terminal_arrival , seat_klass
                    String QueryTravels = "select min(departs_at), max(departs_at), cast(avg(travel_time_minutes) as int),count(schedules_id) from " + Schedules + " where travel_id =" + String.valueOf(ScheduleID) + " and bus_operator_id=" + cursorOperators.getString(0)
                            + " and type=" + type;
                    cursorTravels = dbTravels.rawQuery(QueryTravels, null);
                    ObjectBusOperatorSchedule bop = new ObjectBusOperatorSchedule();
                    if (cursorTravels != null && cursorTravels.getCount() > 0) {
                        while (cursorTravels.moveToNext()) {
                            bop.setFirstDeparture(u.DateTimeToString(u.StringToDate(cursorTravels.getString(0),"/","d/M/y H:m:s"),"HH:mm"));
                            bop.setLastDeparture(u.DateTimeToString(u.StringToDate(cursorTravels.getString(1),"/","d/M/y H:m:s"),"HH:mm"));
                            bop.setNumberOfServices(cursorTravels.getString(3));
                            bop.setObjectBusOperator(getBusOperatorById(cursorOperators.getInt(0)));
                            bop.setTravelTime(u.MinutesToHoursHM(cursorTravels.getInt(2)));
                            bop.setTravelTimeInMinutes(cursorTravels.getInt(2));

                            //
                            bop.setHasSemiCama(false);
                            bop.setHasSalonCama(false);
                            bop.setHasPremium(false);
                            int SemiCamaPrice = this.GetMinPrice(bop.getObjectBusOperator(), ScheduleID, type, 3);
                            bop.setSemiCamaPrice(u.FormatMoney(SemiCamaPrice));
                            if (SemiCamaPrice != 0) {
                                bop.setHasSemiCama(true);
                            }

                            int SalonCamaPrice = this.GetMinPrice(bop.getObjectBusOperator(), ScheduleID, type, 2);
                            bop.setSalonCamaPrice(u.FormatMoney(SalonCamaPrice));
                            if (SalonCamaPrice != 0) {
                                bop.setHasSalonCama(true);
                            }
                            int PremiumPrice = this.GetMinPrice(bop.getObjectBusOperator(), ScheduleID, type, 1);
                            bop.setPremiumPrice(u.FormatMoney(PremiumPrice));
                            if (PremiumPrice != 0) {
                                bop.setHasPremium(true);
                            }


                            collection.add(bop);
                        }
                    }
                }
            }
        } catch (SQLiteException ex) {
            Log.v("SQLiteException ", ex.getMessage());
        }  catch (Exception ex) {
            Log.v("Exception ", ex.getMessage());
        } finally {
            cursorOperators.close();
            dbOperators.close();
        }
        return collection;
    }

    public int GetMinPrice(ObjectBusOperator operator, int travel, String type, int SeatClass) {
        int total = 0;
        Util u = new Util();
        SQLiteDatabase db = this.getReadableDatabase();
        SQLiteStatement s = db.compileStatement("select min(price) from " + Schedules + " where travel_id =" + String.valueOf(travel) + " and bus_operator_id=" + String.valueOf(operator.getIdBusOperator()) + " and type=" + (type) + " and seat_klass_stars=" + String.valueOf(SeatClass));
        long count = s.simpleQueryForLong();
        total = u.safeLongToInt(count);
        return total;
    }

    public ObjectCity getCityById(int CityID) {
        ObjectCity ObjectCity = new ObjectCity();
        Cursor cursor;
        SQLiteDatabase db = this.getReadableDatabase();
        String MY_QUERY = "select id,name from " + TableCities + " WHERE id=" + String.valueOf(CityID);

        cursor = db.rawQuery(MY_QUERY, null);
        try {
            if (cursor != null && cursor.getCount() > 0) {
                while (cursor.moveToNext()) {
                    ObjectCity = new ObjectCity(cursor.getInt(0), cursor.getString(1));

                }
            }
        } catch (SQLiteException ex) {
            Log.v("Exception ", ex.getMessage());
        } finally {
            cursor.close();
            db.close();
        }
        return ObjectCity;
    }

    public ObjectBusOperator getBusOperatorById(int BusOperatorId) {
        ObjectBusOperator objectBusOperator = new ObjectBusOperator();
        Cursor cursor;
        SQLiteDatabase db = this.getReadableDatabase();
        String MY_QUERY = "select IdBusOperator, NameBusOperator,BusOperatorIconUrl from " + TableBusOperator + " WHERE IdBusOperator=" + String.valueOf(BusOperatorId);

        cursor = db.rawQuery(MY_QUERY, null);
        try {
            if (cursor != null && cursor.getCount() > 0) {
                while (cursor.moveToNext()) {
                    objectBusOperator = new ObjectBusOperator(cursor.getInt(0), cursor.getString(1), cursor.getString(2));

                }
            }
        } catch (SQLiteException ex) {
            Log.v("Exception ", ex.getMessage());
        } finally {
            cursor.close();
            db.close();
        }
        return objectBusOperator;
    }
}
