package cl.recorrido.micritodroid;

/**
 * Created by brede on 10-11-14.
 */
public class ObjectBusOperator {
    int IdBusOperator;
    String NameBusOperator;
    String icon_url;

    public String getIcon_url() {
        return icon_url;
    }

    public void setIcon_url(String icon_url) {
        this.icon_url = icon_url;
    }

    public int getIdBusOperator() {
        return IdBusOperator;
    }

    public void setIdBusOperator(int idBusOperator) {
        IdBusOperator = idBusOperator;
    }

    public String getNameBusOperator() {
        return NameBusOperator;
    }

    public void setNameBusOperator(String nameBusOperator) {
        this.NameBusOperator = nameBusOperator;
    }

    public ObjectBusOperator(int idBusOperator) {
        IdBusOperator = idBusOperator;
    }

    public ObjectBusOperator() {
    }

    public ObjectBusOperator(int idBusOperator, String nameBusOperator, String icon_url) {

        IdBusOperator = idBusOperator;
        NameBusOperator = nameBusOperator;
        this.icon_url = icon_url;
    }
}
