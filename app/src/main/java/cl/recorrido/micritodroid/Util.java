package cl.recorrido.micritodroid;

import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;

import org.json.JSONException;
import org.json.JSONObject;

import java.text.DateFormat;
import java.text.NumberFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;

/**
 * Created by brede on 05-11-14.
 */
public class Util {
    public String DateTimeToString(Date date, String Format) {
        DateFormat df = new SimpleDateFormat(Format);
        String sdt = df.format(date);
        return sdt;
    }
    public String CalendarToString(Calendar calendar, String Format) {
        DateFormat df = new SimpleDateFormat(Format);
//        df.setCalendar(calendar);
        return df.format(calendar.getTime());
    }

    //    public Date StringToDatetime(Date date, String Format) {
//        DateFormat df = new SimpleDateFormat(Format);
    //TODO: date with timezone 2014-11-11T23:55:00.000-03:00 to datetime object
//        return df;
//    }
    public String CalendarToString(Calendar date) {
        String cadena = "";
        cadena = String.valueOf(date.get(Calendar.DAY_OF_MONTH)) + "/" + String.valueOf(date.get(Calendar.MONTH) +1) + "/" + String.valueOf(date.get(Calendar.YEAR));
        return cadena;
    }

    public Date StringToDate(String Date, String Separator, String Format) {
        String[] dates = Date.split(Separator);
        Calendar calendar = Calendar.getInstance();
        if (Format.compareTo("d/m/Y") == 0) {
            calendar.set(Integer.parseInt(dates[2].trim()), Integer.parseInt(dates[1].trim()) - 1, Integer.parseInt(dates[0].trim()), 0, 0, 0);
        } else if (Format.compareTo("d/M/y H:m:s") == 0) {
            String[] datestmp = Date.split(" ");
            dates = datestmp[0].split("" + Separator + "");
            String[] times = datestmp[1].split(":");
            calendar.set(Integer.parseInt(dates[2].trim()), Integer.parseInt(dates[1].trim()) - 1, Integer.parseInt(dates[0].trim()), Integer.parseInt(times[0]), Integer.parseInt(times[1]), 0);
        }
        Date dt = new Date();
        dt.setTime(calendar.getTime().getTime());
        return dt;
    }



    public String MinutesToHours(int minutes) {
        String Return = "0:00";
        int remain = minutes % 60;
        int hours = ((minutes - remain) / 60) * 1;
        String strHours = String.valueOf(hours);
        if (hours < 10) {
            strHours = "0".concat(String.valueOf(hours));
        }
        String strMinutes = String.valueOf(remain);
        if (remain < 10) {
            strMinutes = "0".concat(String.valueOf(remain));
        }
        Return = strHours + ":" + strMinutes;
        return Return;
    }

    public String MinutesToHoursHM(int minutes) {
        String Return = "0:00";
        int remain = minutes % 60;
        int hours = ((minutes - remain) / 60) * 1;
        String strHours = String.valueOf(hours);
        if (hours < 10) {
            strHours = "0".concat(String.valueOf(hours));
        }
        String strMinutes = String.valueOf(remain);
        if (remain < 10) {
            strMinutes = "0".concat(String.valueOf(remain));
        }
        Return = strHours + "H " + strMinutes + "M";
        if (hours == 0 && remain == 0) {
            Return = "";
        }
        return Return;
    }

    public boolean DateCompare(Date Initial, Date Last) {
        Calendar DepartureTemp = Calendar.getInstance();
        DepartureTemp.setTime(Initial);
        Calendar DestinyTemp = Calendar.getInstance();
        DestinyTemp.setTime(Last);

        if (DepartureTemp.compareTo(DestinyTemp)<0) {
            return false;
        } else {
            return true;
        }
    }

    /**
     * This function adds Time
     *
     * @return
     */
    public Date AddTime(Date date, int field, int amount) {
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(date);
        calendar.roll(field, amount);
        Date dt = new Date();
        dt.setTime(calendar.getTime().getTime());
        return dt;
    }
    public Date SetTime(Date date, int field, int amount) {
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(date);
        calendar.set(field, amount);
        Date dt = new Date();
        dt.setTime(calendar.getTime().getTime());
        return dt;
    }
    public Date AddDays(Date date, boolean increment) {
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(date);
        calendar.roll(Calendar.DAY_OF_MONTH, increment);
        Date dt = new Date();
        dt.setTime(calendar.getTime().getTime());
        return dt;
    }

    public int BoolToInt(boolean b) {
        if (b)
            return 1;
        return 0;
    }

    public boolean IntToBool(int n) {
        if (n == 1)
            return true;
        return false;
    }

    public String FormatMoney(int money) {
        Locale cl = new Locale("es_CL", "es_CL");
        NumberFormat dFormat = NumberFormat.getCurrencyInstance(cl);
//        NumberFormat formatter = NumberFormat.getCurrencyInstance();
        return (dFormat.format(money));
    }

    public int BusType(int n) {
        switch (n) {
            case 3:
                return R.string.SemiCama;
            case 2:
                return R.string.SalonCama;
            case 1:
                return R.string.Premium;
        }
        return R.string.ordering;
    }

    public static int safeLongToInt(long l) {
        if (l < Integer.MIN_VALUE || l > Integer.MAX_VALUE) {
            throw new IllegalArgumentException
                    (l + " cannot be cast to int without changing its value.");
        }
        return (int) l;
    }

    public boolean isNetworkAvailable(Context context) {
        ConnectivityManager connectivityManager
                = (ConnectivityManager) context.getSystemService(context.CONNECTIVITY_SERVICE);
        NetworkInfo activeNetworkInfo = connectivityManager.getActiveNetworkInfo();
        return activeNetworkInfo != null && activeNetworkInfo.isConnected();
    }

    public int KeyValueError(int code) {
        switch (code) {
            case 400:
                return R.string.Error400;
            case 401:
                return R.string.Error401;
            case 403:
                return R.string.Error403;
            case 404:
                return R.string.Error404;
            case 405:
                return R.string.Error405;
            case 409:
                return R.string.Error409;
            case 412:
                return R.string.Error412;
            case 413:
                return R.string.Error413;
            case 422:
                return R.string.Error422;
            case 500:
                return R.string.Error500;
            case 501:
                return R.string.Error501;
            case 503:
                return R.string.Error503;
            default:
                return R.string.ErrorXX;
        }
    }
    public String CalendarIntToString(String Separator, int FieldDateTmp, int FieldDate, int DepartureDay, int DepartureMonth, int DepartureYear, int ReturnDay, int ReturnMonth, int ReturnYear) {
        int tmpDate = FieldDate;
        String cadena = "";
        if (FieldDateTmp != 0) {
            tmpDate = FieldDateTmp;
        }
        if (tmpDate == 1) {
            cadena = String.valueOf(DepartureDay) + Separator + String.valueOf(DepartureMonth) + Separator + String.valueOf(DepartureYear);
        } else {
            cadena = String.valueOf(ReturnDay) + Separator + String.valueOf(ReturnMonth) + Separator + String.valueOf(ReturnYear);
        }
        return cadena;
    }

    public int JsonToInt(JSONObject JSONObs, String Campo) {
        int number = 0;
        if (!JSONObs.isNull(Campo)) {
            try {
                number = JSONObs.getInt(Campo);
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
        return number;
    }
}
