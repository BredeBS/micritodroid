package cl.recorrido.micritodroid;


import android.app.ProgressDialog;
import android.content.Context;
import android.content.SharedPreferences;
import android.graphics.Typeface;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.os.Handler;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AbsListView;
import android.widget.AdapterView;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.ToggleButton;

import com.nostra13.universalimageloader.core.ImageLoader;

import java.util.Calendar;
import java.util.Date;


/**
 * A simple {@link Fragment} subclass.
 */
public class FragmentResults extends Fragment {

    SharedPreferences settings;
    Connection cn;
    ListView SchedulesList;
    ObjectBusTravel travel;
    DB db;
    public ImageLoader imageLoader;
    ProgressDialog progressDialog;
    int intRating, intDepartureTime, intFare;
    Calendar tmpDateReturn, tmpDateDeparture, Today, Yesterday, Tomorrow;
    TextView departureDate, txtRate, txtDepartureTime, txtFare, travelCities, txtPrevDay, txtNextDay;
    Util u;
    LinearLayout divRate, divDepartureTime, divFare, btnDayLess, btnDayMore, FilterTop;
    ImageView imgRate, imgDepartureTime, imgFare;
    AdapterOperatorSchedule ScheduleAdapter;
    int TravelID = 0;// TODO set this to 0
    String TravelType = "inbound";  //outbound = 1; inbound = 2;
    Drawable imgUp;
    Drawable imgDown;
    Boolean RoundTrip;
    ToggleButton tbsemi,tbcama, tbpremium;
    Handler uiHandler;
    ActivityMain activity;

    public FragmentResults() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_results, container, false);
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        activity = (ActivityMain) getActivity();
        settings = activity.getSharedPreferences("cl.recorrido.micritodroid", Context.MODE_PRIVATE);
        uiHandler = new Handler();

//      TODO: Uncomment this to retrieve right number
        TravelID = Integer.parseInt(settings.getString("SearchID", ""));
        TravelType = (settings.getString("TravelType", ""));
        RoundTrip = Boolean.getBoolean(settings.getString("RoundTrip", ""));

        db = new DB(activity.getApplicationContext());
        cn = new Connection();
        u = new Util();
        //preload images
        String uriUp = "@drawable/up176";
        int imageResourceUp = getResources().getIdentifier(uriUp, null, activity.getPackageName());
        imgUp = getResources().getDrawable(imageResourceUp);
        String uriDown = "@drawable/downwards";
        int imageResourceDown = getResources().getIdentifier(uriDown, null, activity.getPackageName());
        imgDown = getResources().getDrawable(imageResourceDown);
        //!preload
        //textview
        departureDate = (TextView) activity.findViewById(R.id.departureDate);
        txtPrevDay = (TextView) activity.findViewById(R.id.txtPrevDay);
        txtNextDay = (TextView) activity.findViewById(R.id.txtNextDay);
        txtRate = (TextView) activity.findViewById(R.id.txtRate);
        travelCities = (TextView) activity.findViewById(R.id.travelCities);
        txtDepartureTime = (TextView) activity.findViewById(R.id.txtDepartureTime);
        txtFare = (TextView) activity.findViewById(R.id.txtFare);
        //Toggle Button
        tbsemi = (ToggleButton) activity.findViewById(R.id.tbsemi);
        tbcama = (ToggleButton) activity.findViewById(R.id.tbcama);
        tbpremium = (ToggleButton) activity.findViewById(R.id.tbpremium);
        tbsemi.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                activity.SearchSemi = tbsemi.isChecked();
            }
        });
        tbcama.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                activity.SearchCama = tbcama.isChecked();
            }
        });
        tbpremium.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                activity.SearchPremium = tbpremium.isChecked();
            }
        });
        //Divs

        btnDayLess = (LinearLayout) activity.findViewById(R.id.btnDayLess);
        FilterTop = (LinearLayout) activity.findViewById(R.id.FilterTop);
        btnDayMore = (LinearLayout) activity.findViewById(R.id.btnDayMore);
        divRate = (LinearLayout) activity.findViewById(R.id.divRate);
        divDepartureTime = (LinearLayout) activity.findViewById(R.id.divDepartureTime);
        divFare = (LinearLayout) activity.findViewById(R.id.divFare);
        //Img
        imgRate = (ImageView) activity.findViewById(R.id.imgRate);
        imgDepartureTime = (ImageView) activity.findViewById(R.id.imgDepartureTime);
        imgFare = (ImageView) activity.findViewById(R.id.imgFare);
        imgRate.setVisibility(View.INVISIBLE);
        imgDepartureTime.setVisibility(View.INVISIBLE);
        imgFare.setVisibility(View.INVISIBLE);
        FilterTop.setVisibility(View.INVISIBLE);

        Today = Calendar.getInstance();
        Today.set(Calendar.HOUR_OF_DAY, 0);
        Today.set(Calendar.MINUTE, 0);
        Today.set(Calendar.SECOND, 0);
//        Yesterday = u.AddDays(Today, false);

//        Today = Today.setDate();


//        activity.DateDeparture = u.IntToDate(activity.DepartureYear, activity.DepartureMonth - 1, activity.DepartureDay, 0, 0, 0);
//        activity.DateReturn = u.IntToDate(activity.ReturnYear, activity.ReturnMonth - 1, activity.ReturnDay, 23, 59, 59);
        ChangeYesterday();
        intRating = 1;
        intDepartureTime = 0;
        intFare = 0;
        LoadSchedules();
        divRate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (intRating == 1)
                    intRating = 2;
                else
                    intRating = 1;
                intDepartureTime = 0;
                intFare = 0;
                LoadSchedules();
            }
        });
        divDepartureTime.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (intDepartureTime == 1)
                    intDepartureTime = 2;
                else
                    intDepartureTime = 1;
                intRating = 0;
                intFare = 0;
                LoadSchedules();
            }
        });
        divFare.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (intFare == 1)
                    intFare = 2;
                else
                    intFare = 1;
                intRating = 0;
                intDepartureTime = 0;
                LoadSchedules();
            }
        });
        btnDayLess.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                AddTimeDeparture(-1);
            }
        });
        btnDayMore.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                AddTimeDeparture(1);
            }
        });
    }

    public void AddTimeDeparture(int increment) {
        //TODO Detect Departure /  Return
        boolean valid = true;
        tmpDateDeparture = activity.DateDeparture;
        tmpDateDeparture.add(Calendar.DAY_OF_MONTH, increment);
        if (increment < 0) {
            valid = tmpDateDeparture.after(Today);
        }
        if (valid) {
            if (activity.IsRoundTrip) {
                if (activity.DateReturn.after(tmpDateDeparture)) {
                    ChangeDate();
                } else {
                    activity.Message(activity.getText(R.string.ReturnAfterTrip).toString());

                }
            } else {
                ChangeDate();
            }
        } else {
            activity.Message(activity.getText(R.string.DepartureDayAfterToday).toString());
        }
    }

    public void ChangeDate() {
        if (TravelType.compareTo("outbound") == 0) {
            activity.DateDeparture = tmpDateDeparture;
        } else {
            activity.DateReturn = tmpDateReturn;
        }

        ChangeYesterday();

//        activity.SearchTravel();
    }

    public void Images(ImageView Enabled, ImageView DisabledA, ImageView DisabledB, int type) {
        Enabled.setVisibility(View.VISIBLE);
        DisabledA.setVisibility(View.INVISIBLE);
        DisabledB.setVisibility(View.INVISIBLE);
        if (type == 1) {
            Enabled.setImageDrawable(imgDown);
        } else {
            Enabled.setImageDrawable(imgUp);
        }
    }

    public void TextViews(TextView Hightlighted, TextView NormalA, TextView NormalB) {
        Hightlighted.setTypeface(null, Typeface.BOLD);
        NormalA.setTypeface(null, Typeface.NORMAL);
        NormalB.setTypeface(null, Typeface.NORMAL);
    }

    public void ChangeYesterday() {
//TODO REvisar esto!!!!!! no cambia fecha
        if (TravelType.compareTo("outbound") == 0) {
            Yesterday = (Calendar) activity.DateDeparture.clone();
            Tomorrow = (Calendar) activity.DateDeparture.clone();
            departureDate.setText(u.CalendarToString(activity.DateDeparture, "EE dd MMMM yyyy"));
        } else {
            Yesterday = (Calendar) activity.DateReturn.clone();
            Tomorrow = (Calendar) activity.DateReturn.clone();
            departureDate.setText(u.CalendarToString(activity.DateReturn, "EE dd MMMM yyyy"));
        }
        Log.v("date: yesterday", u.CalendarToString(Yesterday, "dd/MM/yyyy"));
        Log.v("date: tomorrow", u.CalendarToString(Tomorrow, "dd/MM/yyyy"));
        Yesterday.add(Calendar.DAY_OF_MONTH, -1);
        Log.v("date: yesterday -1", u.CalendarToString(Yesterday, "dd/MM/yyyy"));
        Tomorrow.add(Calendar.DAY_OF_MONTH, 1);

        Log.v("date: tomorrow +1", u.CalendarToString(Tomorrow, "dd/MM/yyyy"));
        txtNextDay.setText(u.CalendarToString(Tomorrow, "dd/MM"));
        txtPrevDay.setText(u.CalendarToString(Yesterday, "dd/MM"));
        if (Yesterday.before(Today)) {
            txtPrevDay.setTextAppearance(activity, R.style.SearchTextBoxOverOff);
        } else {
            txtPrevDay.setTextAppearance(activity, R.style.SearchTextBoxOver);
        }
    }

    public void LoadSchedules() {
        //TODO: Uncomment this
//        activity.runOnUiThread(new Runnable() {
//
//            @Override
//            public void run() {
//                if (progressDialog == null) {
//                    progressDialog = new ProgressDialog(activity);
//                    progressDialog.setMessage(activity.getBaseContext().getString(R.string.ordering));
//                    progressDialog.show();
//                    progressDialog.setCanceledOnTouchOutside(false);
//                    progressDialog.setCancelable(false);
//                }
//            }
//        });
        if (intRating == 1 || intRating == 2) {
            TextViews(txtRate, txtDepartureTime, txtFare);
            Images(imgRate, imgDepartureTime, imgFare, intRating);
        } else if (intDepartureTime == 1 || intDepartureTime == 2) {
            TextViews(txtDepartureTime, txtRate, txtFare);
            Images(imgDepartureTime, imgRate, imgFare, intDepartureTime);
        } else if (intFare == 1 || intFare == 2) {
            TextViews(txtFare, txtDepartureTime, txtRate);
            Images(imgFare, imgDepartureTime, imgRate, intFare);
        }

        travel = db.getOperatorTravel(TravelID, TravelType, intRating, intDepartureTime, intFare);
        if (travel.getBusOperator().size() > 0) {
            SchedulesList = (ListView) activity.findViewById(R.id.listSchedules);

            SchedulesList.setClickable(false);

//        db = new DB(this);



            travelCities.setText(travel.getDepartureCity().getName().concat(" - ").concat(travel.getDestinationCity().getName()));

            ScheduleAdapter = new AdapterOperatorSchedule(activity, R.layout.extra_row_country, travel.getBusOperator());
            SchedulesList.setAdapter(ScheduleAdapter);
            SchedulesList.setChoiceMode(AbsListView.CHOICE_MODE_SINGLE);
            SchedulesList.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                @Override
                public void onItemClick(AdapterView<?> parent, View view, int position,
                                        long id) {
                    if (travel.isRoundTrip() && TravelType.compareTo("outbound") == 0) {
                        SharedPreferences settings = activity.getSharedPreferences("cl.recorrido.micritodroid", Context.MODE_PRIVATE);
                        SharedPreferences.Editor editor = settings.edit();
                        ObjectBusSchedule Schedule = travel.getSchedule().get(position);
                        editor.putString("TravelType", "inbound");
                        if (TravelType.compareTo("outbound") == 0) {
                            editor.putString("outboundTravelID", String.valueOf(Schedule.getId()));
                        } else {
                            editor.putString("inboundTravelID", String.valueOf(Schedule.getId()));
                        }
                        editor.commit();
//                        Intent myIntent = new Intent(activity, ActivityResults.class);
//                        startActivity(myIntent);
                        activity.ShowFragment(activity.OPTION_RESULTS);
                    } else {

                    }
                }
            });
            activity.runOnUiThread(new Runnable() {

                @Override
                public void run() {
                    if (progressDialog.isShowing()) {
                        progressDialog.dismiss();
                        progressDialog = null;
                    }
                }
            });
        } else {
            activity.Message(activity.getBaseContext().getText(R.string.MisteriousError).toString());
        }
    }
}
