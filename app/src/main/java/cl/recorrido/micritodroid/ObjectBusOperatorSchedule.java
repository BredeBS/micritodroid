package cl.recorrido.micritodroid;

/**
 * Created by brede on 11-11-14.
 */
public class ObjectBusOperatorSchedule {
    String SemiCamaPrice;
    String SalonCamaPrice;
    String PremiumPrice;
    String NumberOfServices;
    Boolean HasSemiCama;
    Boolean HasSalonCama;
    Boolean HasPremium;
    ObjectBusOperator objectBusOperator;
    String FirstDeparture;
    String LastDeparture;
    String TravelTime;
    int TravelTimeInMinutes;

    public ObjectBusOperatorSchedule() {
    }

    public String getSemiCamaPrice() {
        return SemiCamaPrice;
    }

    public void setSemiCamaPrice(String semiCamaPrice) {
        SemiCamaPrice = semiCamaPrice;
    }

    public String getSalonCamaPrice() {
        return SalonCamaPrice;
    }

    public void setSalonCamaPrice(String salonCamaPrice) {
        SalonCamaPrice = salonCamaPrice;
    }

    public String getPremiumPrice() {
        return PremiumPrice;
    }

    public void setPremiumPrice(String premiumPrice) {
        PremiumPrice = premiumPrice;
    }

    public String getNumberOfServices() {
        return NumberOfServices;
    }

    public void setNumberOfServices(String numberOfServices) {
        NumberOfServices = numberOfServices;
    }

    public Boolean getHasSemiCama() {
        return HasSemiCama;
    }

    public void setHasSemiCama(Boolean hasSemiCama) {
        HasSemiCama = hasSemiCama;
    }

    public Boolean getHasSalonCama() {
        return HasSalonCama;
    }

    public void setHasSalonCama(Boolean hasSalonCama) {
        HasSalonCama = hasSalonCama;
    }

    public Boolean getHasPremium() {
        return HasPremium;
    }

    public void setHasPremium(Boolean hasPremium) {
        HasPremium = hasPremium;
    }

    public ObjectBusOperator getObjectBusOperator() {
        return objectBusOperator;
    }

    public void setObjectBusOperator(ObjectBusOperator objectBusOperator) {
        this.objectBusOperator = objectBusOperator;
    }

    public String getFirstDeparture() {
        return FirstDeparture;
    }

    public void setFirstDeparture(String firstDeparture) {
        FirstDeparture = firstDeparture;
    }

    public String getLastDeparture() {
        return LastDeparture;
    }

    public void setLastDeparture(String lastDeparture) {
        LastDeparture = lastDeparture;
    }

    public String getTravelTime() {
        return TravelTime;
    }

    public void setTravelTime(String travelTime) {
        TravelTime = travelTime;
    }

    public int getTravelTimeInMinutes() {
        return TravelTimeInMinutes;
    }

    public void setTravelTimeInMinutes(int travelTimeInMinutes) {
        TravelTimeInMinutes = travelTimeInMinutes;
    }
}
