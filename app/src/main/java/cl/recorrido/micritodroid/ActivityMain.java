package cl.recorrido.micritodroid;


import android.content.Context;
import android.content.SharedPreferences;
import android.content.res.Configuration;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.AsyncTask;
import android.os.Handler;
import android.os.StrictMode;
import android.support.v4.app.ActionBarDrawerToggle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;

import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.text.Html;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.view.Window;
import android.widget.AdapterView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.ImageLoaderConfiguration;

import net.danlew.android.joda.JodaTimeAndroid;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;


public class ActivityMain extends ActionBarActivity {
    DrawerLayout NavDrawerLayout;
    final int OPTION_SEARCH = 2;
    final int OPTION_RESULTS = 100;
    final int OPTION_RESULTS_OUTBOUND = 101;
    Calendar DateDeparture, DateReturn;
    ObjectState BusTravelStatus;

    ProgressBar ProgressBarSearching;
    TextView Percentage;
    final int OPTION_RESULTS_INBOUND = 102;
    DB db;
    private ActionBarDrawerToggle mDrawerToggle;
    Connection cn;
    ListView mDrawerList;
    int CityDepartureId, CityDestinyId, FieldDate, IsOpened, DepartureCountryID, DestinationCountryID, totalTravel, ProgressID = 0;
    Util u;
    private AdapterNavigation NavAdapter;
    LinearLayout ViewSearching;
    CharSequence mTitle, mDrawerTitle;
    private String[] titulos;
    private ArrayList<ObjectNavigation> NavItms;
    boolean IsRoundTrip = false;
    boolean SearchCama,SearchSemi,SearchPremium=true;

    public Handler getUiHandler() {
        return uiHandler;
    }

    Handler uiHandler;

    public ImageLoader imageLoader;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        getWindow().requestFeature(Window.FEATURE_ACTION_BAR);
        super.onCreate(savedInstanceState);
        u = new Util();
        DateDeparture = Calendar.getInstance();
        DateReturn = Calendar.getInstance();
        JodaTimeAndroid.init(this);
        db = new DB(getApplicationContext());
        uiHandler = new Handler();
        setContentView(R.layout.activity_main);
        ImageLoaderConfiguration config = new ImageLoaderConfiguration.Builder(getApplicationContext())
                .threadPriority(Thread.NORM_PRIORITY - 2)
                .denyCacheImageMultipleSizesInMemory()
                .diskCacheSize(50 * 1024 * 1024) // 50 Mb
                .imageDownloader(new AuthImageDownloader(getApplicationContext(), 10000, 10000))
                .writeDebugLogs() // Remove for release app
                .build();
        ImageLoader.getInstance().init(config);
        setOptions();
        cn = new Connection();
        imageLoader = ImageLoader.getInstance();
        ViewSearching = (LinearLayout) findViewById(R.id.ViewSearching);
        ProgressBarSearching = (ProgressBar) findViewById(R.id.ProgressBarSearching);
        Percentage = (TextView) findViewById(R.id.Percentage);
        ResetProgress(0);
        ViewSearching.setVisibility(View.INVISIBLE);
        NavDrawerLayout = (DrawerLayout) findViewById(R.id.drawer_layout);
        mDrawerList = (ListView) findViewById(R.id.left_drawer);
        NavDrawerLayout = (DrawerLayout) findViewById(R.id.drawer_layout);
        View header = getLayoutInflater().inflate(R.layout.extra_navigation_header, null);
        mDrawerList.addHeaderView(header);
        View footer = getLayoutInflater().inflate(R.layout.extra_navigation_footer, null);
        mDrawerList.addFooterView(footer);
        titulos = getResources().getStringArray(R.array.nav_options);
        NavItms = new ArrayList<ObjectNavigation>();
        NavAdapter = new AdapterNavigation(this, NavItms);
        mDrawerList.setAdapter(NavAdapter);
        //Siempre vamos a mostrar el mismo titulo
        mTitle = mDrawerTitle = getTitle();
        Percentage.setText("0%");

        //Declaramos el mDrawerToggle y las imgs a utilizar
        mDrawerToggle = new ActionBarDrawerToggle(
                this,                  /* host Activity */
                NavDrawerLayout,         /* DrawerLayout object */
                R.drawable.icon_toggle,  /* Icono de navegacion*/
                R.string.app_name,  /* "open drawer" description */
                R.string.Close  /* "close drawer" description */
        ) {

            /** Called when a drawer has settled in a completely closed state. */
            public void onDrawerClosed(View view) {


                Log.e("Cerrado completo", "!!");
            }

            @Override
            public void onDrawerSlide(View drawerView, float slideOffset) {
                if (slideOffset == 0) {
                    getSupportActionBar().setIcon(R.drawable.logo_close);
                } else if (slideOffset != 0) {
                    getSupportActionBar().setIcon(R.drawable.logo_min);
                }
                super.onDrawerSlide(drawerView, slideOffset);
            }

            public void onDrawerOpened(View drawerView) {
                Log.e("Apertura completa", "!!");
            }
        };
        setTitleActionBar(getApplicationContext().getText(R.string.app_name).toString());
        NavDrawerLayout.setDrawerListener(mDrawerToggle);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        getSupportActionBar().setHomeButtonEnabled(true);
        getSupportActionBar().setBackgroundDrawable(new ColorDrawable(0xff69c2c3));
        getSupportActionBar().setIcon(R.drawable.logo_close);
        mDrawerList.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> arg0, View arg1, int position, long id) {
                Log.v("posicion", String.valueOf(position));
                ShowFragment(position);
            }
        });
        //TODO: Remove comments and OPTION_RESULTS
//        ShowFragment(OPTION_SEARCH);
        ShowFragment(OPTION_RESULTS);
    }

    public ImageLoader getImageLoader() {
        return imageLoader;
    }

    public void setTitleActionBar(String Message) {
        getSupportActionBar().setTitle(Html.fromHtml("<font color=\"#ffffff\">" + Message + "</font>"));
    }

    public void Message(final String Message) {
        uiHandler.post(new Runnable() {
            public void run() {
                Toast.makeText(getApplicationContext(), Message, Toast.LENGTH_SHORT).show();
            }
        });
    }

    DisplayImageOptions options;

    public DisplayImageOptions getOptions() {
        return options;
    }

    public void setOptions() {
        this.options = new DisplayImageOptions.Builder()
                .showImageOnFail(R.drawable.chile)
                .cacheInMemory(true)
                .cacheOnDisk(true)
                .considerExifParams(true)
                .bitmapConfig(Bitmap.Config.RGB_565)
                .build();

    }

    //////////
    public void ShowFragment(int position) {
        Fragment fragment = null;
        boolean Fragmento = false;
        switch (position) {
            case OPTION_SEARCH: {
                fragment = new FragmentSearch();
                setTitleActionBar(getBaseContext().getText(R.string.titleSearch).toString());
                Fragmento = true;
                break;

            }
            case OPTION_RESULTS: {
                fragment = new FragmentResults();
                setTitleActionBar(getBaseContext().getText(R.string.titleResults).toString());
                Fragmento = true;
                break;

            }
            case OPTION_RESULTS_OUTBOUND: {
                fragment = new FragmentResults();
                setTitleActionBar(getBaseContext().getText(R.string.titleResultsOutbound).toString());
                Fragmento = true;
                break;

            }
            case OPTION_RESULTS_INBOUND: {
                fragment = new FragmentResults();
                setTitleActionBar(getBaseContext().getText(R.string.titleResultsInbound).toString());
                Fragmento = true;
                break;

            }
            default:
                Message("Opción no disponible!");

                break;
        }
        if (Fragmento) {
            if (fragment != null) {
                FragmentManager fragmentManager = getSupportFragmentManager();

                fragmentManager.beginTransaction().replace(R.id.content_frame, fragment).commit();
                NavDrawerLayout.closeDrawer(mDrawerList);
            } else {
                Log.e("Error  ", "ShowFragment " + position);
            }
        } else {
            Log.e("Mostrar ", "Mostrar Tarjetas");
        }
    }

    @Override
    protected void onPostCreate(Bundle savedInstanceState) {
        super.onPostCreate(savedInstanceState);
        mDrawerToggle.syncState();
    }

    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);
        mDrawerToggle.onConfigurationChanged(newConfig);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (mDrawerToggle.onOptionsItemSelected(item)) {
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onBackPressed() {
        return;
    }

    // utils for search
    public boolean DateCompare() {
        if (DateDeparture.compareTo(DateReturn) > 0 && IsRoundTrip && IsOpened == 1) {
            return true;
        } else {
            IsOpened = 0;
            return false;
        }
    }

    public void EndSearch(boolean State, int number) {
        if (State) {
            SharedPreferences settings = getSharedPreferences("cl.recorrido.micritodroid", Context.MODE_PRIVATE);
            ProgressID = Integer.parseInt(settings.getString("SearchID", ""));
            totalTravel += db.TotalSchedules(ProgressID, "outbound");
            totalTravel += db.TotalSchedules(ProgressID, "inbound");
            if (totalTravel != 0) {
                SharedPreferences.Editor editor = settings.edit();
                editor.putString("SearchID", String.valueOf(number));
                editor.commit();
                if (IsRoundTrip)
                    ShowFragment(OPTION_RESULTS_OUTBOUND);
                else {
                    ShowFragment(OPTION_RESULTS);
                }
            } else {
                getUiHandler().post(new Runnable() {
                    public void run() {
                        ViewSearching.setVisibility(View.INVISIBLE);
                    }
                });
                Message(getBaseContext().getText(R.string.NoResults).toString());
            }
        } else {
            Message(getBaseContext().getText(R.string.SearchingError).toString());
            getUiHandler().post(new Runnable() {
                public void run() {
                    ViewSearching.setVisibility(View.INVISIBLE);
                }
            });
        }
    }

    public void ResetProgress(int Progress) {
        ProgressBarSearching.setMax(100);
        ProgressBarSearching.setProgress(Progress);
//        ProgressBarSearching.setBackground(getResources().getDrawable(R.drawable.progress_bg_recorrido));
        ProgressBarSearching.getIndeterminateDrawable().setColorFilter(
                Color.parseColor("#69c2c3"),
                android.graphics.PorterDuff.Mode.SRC_IN);
        ProgressBarSearching.setIndeterminate(true);
        ViewSearching.setVisibility(View.VISIBLE);
        Percentage.setText("0%");
    }

    public void SearchTravel() {
        if (u.isNetworkAvailable(getBaseContext())) {
            if (CityDestinyId == 0 || CityDepartureId == 0) {
                Message(getText(R.string.CompleteFields).toString());
                return;
            }
            try {
                new AsyncTask<Void, Void, Void>() {
                    @Override
                    protected void onPreExecute() {
                        if (ViewSearching.getVisibility() == View.INVISIBLE) {
                            ResetProgress(0);
                        }
                    }

                    @Override
                    protected void onProgressUpdate(Void... values) {
                        super.onProgressUpdate(values);
                    }

                    @Override
                    protected Void doInBackground(Void... params) {
                        try {
                            StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
                            StrictMode.setThreadPolicy(policy);
                            BusTravelStatus = cn.BusTravels(getBaseContext(), getString(R.string.url_base), String.valueOf(CityDepartureId), String.valueOf(CityDestinyId), u.CalendarToString(DateDeparture, "dd/MM/yyyy"), u.CalendarToString(DateReturn, "dd/MM/yyyy"), IsRoundTrip);

                        } catch (IOException e) {
                            e.printStackTrace();
                        }
                        return null;
                    }


                    @Override
                    protected void onPostExecute(Void result) {
                        if (!BusTravelStatus.isKeepWaiting()) {
                            if (ViewSearching.getVisibility() == View.VISIBLE) {
                                getUiHandler().post(new Runnable() {
                                    public void run() {
                                        ResetProgress(100);
                                    }
                                });
                                ViewSearching.setVisibility(View.INVISIBLE);
                                ProgressID = BusTravelStatus.getNumberJustInCase();
                                EndSearch(BusTravelStatus.isState(), BusTravelStatus.getNumberJustInCase());
                            }
                        } else {
                            getUiHandler().post(new Runnable() {
                                public void run() {
                                    ProgressBarSearching.setMax(100);
                                    ProgressBarSearching.setIndeterminate(false);
                                    Percentage.setText(String.valueOf(BusTravelStatus.getProgress()).concat("%"));
                                    ProgressBarSearching.setProgress(BusTravelStatus.getProgress());

                                    ViewSearching.setVisibility(View.VISIBLE);
                                }
                            });
                            ProgressID = BusTravelStatus.getNumberJustInCase();
                            SearchProgress();
                        }
                    }
                }.execute();
            } catch (Exception e) {
                e.printStackTrace();
            }
        } else {
            Message(getBaseContext().getText(R.string.NoConnection).toString());
        }
    }

    public void SearchProgress() {
        try {
            new AsyncTask<Void, Void, Void>() {
                @Override
                protected void onPreExecute() {
                    if (ViewSearching.getVisibility() == View.INVISIBLE) {
                        ResetProgress(0);
                    }
                }

                @Override
                protected Void doInBackground(Void... params) {
                    try {
                        StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
                        StrictMode.setThreadPolicy(policy);
                        BusTravelStatus = cn.BusTravelsProgress(getBaseContext(), getString(R.string.url_base), (ProgressID));
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                    return null;
                }

                @Override
                protected void onProgressUpdate(Void... values) {
                    super.onProgressUpdate(values);
                }

                @Override
                protected void onPostExecute(Void result) {
                    if (!BusTravelStatus.isKeepWaiting()) {
                        if (ViewSearching.getVisibility() == View.VISIBLE) {
//                            CityDestiny.setText(R.string.Select);
                            getUiHandler().post(new Runnable() {
                                public void run() {
                                    ResetProgress(100);
                                }
                            });
                            EndSearch(BusTravelStatus.isState(), BusTravelStatus.getNumberJustInCase());
                        }
                    } else {
                        getUiHandler().post(new Runnable() {
                            public void run() {
                                ProgressBarSearching.setMax(100);
                                ProgressBarSearching.setProgress(BusTravelStatus.getProgress());
                                ProgressBarSearching.setIndeterminate(false);
                                ViewSearching.setVisibility(View.VISIBLE);
                                Percentage.setText(String.valueOf(BusTravelStatus.getProgress()).concat("%"));
                            }
                        });
                        SearchProgress();
                    }
                }
            }.execute();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    //!utils for search
}

