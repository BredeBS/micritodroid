
package cl.recorrido.micritodroid;

import android.content.Context;
import android.graphics.Bitmap;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.nostra13.universalimageloader.core.assist.FailReason;
import com.nostra13.universalimageloader.core.listener.ImageLoadingProgressListener;
import com.nostra13.universalimageloader.core.listener.SimpleImageLoadingListener;

import java.util.ArrayList;

public class AdapterOperatorSchedule extends ArrayAdapter<ObjectBusOperatorSchedule> {
    Util u;
    Context context;
    int layoutResourceId;
    ArrayList<ObjectBusOperatorSchedule> data = null;
    ActivityMain activity;

    public AdapterOperatorSchedule(Context context, int layoutResourceId, ArrayList<ObjectBusOperatorSchedule> data) {
        super(context, layoutResourceId, data);
        this.layoutResourceId = layoutResourceId;
        this.context = context;
        this.data = data;
        activity = (ActivityMain) context;
    }

    public void changeData(ArrayList<ObjectBusOperatorSchedule> data) {
        this.data = data;
        notifyDataSetChanged();
    }

    ContainerSchedule holder = null;
    View row;

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        row = convertView;
        u = new Util();
        final ObjectBusOperatorSchedule object = data.get(position);
        if (row == null) {
            LayoutInflater inflater = ((ActivityMain) context).getLayoutInflater();
            row = inflater.inflate(R.layout.extra_row_operator_schedule, null);
            holder = new ContainerSchedule();
            holder.NumberOfServices = (TextView) row.findViewById(R.id.NumberOfServices);
            holder.TravelTimeAverage = (TextView) row.findViewById(R.id.TravelTimeAverage);
            holder.FirstDeparture = (TextView) row.findViewById(R.id.FirstDeparture);
            holder.LastDeparture = (TextView) row.findViewById(R.id.LastDeparture);
            holder.SemiCamaClass = (TextView) row.findViewById(R.id.SemiCamaClass);
            holder.SemiCamaLowerPrice = (TextView) row.findViewById(R.id.SemiCamaLowerPrice);

            holder.SemiCamaPrice = (TextView) row.findViewById(R.id.SemiCamaPrice);
            holder.SalonCamaClass = (TextView) row.findViewById(R.id.SalonCamaClass);
            holder.SalonCamaLowerPrice = (TextView) row.findViewById(R.id.SalonCamaLowerPrice);
            holder.SalonCamaPrice = (TextView) row.findViewById(R.id.SalonCamaPrice);
            holder.PremiumClass = (TextView) row.findViewById(R.id.PremiumClass);
            holder.PremiumLowerPrice = (TextView) row.findViewById(R.id.PremiumLowerPrice);
            holder.PremiumPrice = (TextView) row.findViewById(R.id.PremiumPrice);
            holder.imageOperatorLogo = (ImageView) row.findViewById(R.id.imageOperatorLogo);

            row.setTag(holder);
        } else {
            holder = (ContainerSchedule) row.getTag();
        }
        holder.TravelTimeAverage.setText(object.getTravelTime());
        holder.NumberOfServices.setText(object.getNumberOfServices());
        holder.FirstDeparture.setText(context.getString(R.string.FirstDeparture).concat(":  ").concat(object.getFirstDeparture()));
        holder.LastDeparture.setText(context.getString(R.string.LastDeparture).concat(":  ").concat(object.getLastDeparture()));
//        holder.LastDeparture.setText(object.getLastDeparture());//@string/LastDeparture
//        holder.LastDeparture.setText(object.getLastDeparture());

        //hide all
        holder.SemiCamaClass.setVisibility(View.INVISIBLE);
        holder.SemiCamaLowerPrice.setVisibility(View.INVISIBLE);
        holder.SemiCamaPrice.setVisibility(View.INVISIBLE);
        holder.SalonCamaClass.setVisibility(View.INVISIBLE);
        holder.SalonCamaLowerPrice.setVisibility(View.INVISIBLE);
        holder.SalonCamaPrice.setVisibility(View.INVISIBLE);
        holder.PremiumClass.setVisibility(View.INVISIBLE);
        holder.PremiumLowerPrice.setVisibility(View.INVISIBLE);
        holder.PremiumPrice.setVisibility(View.INVISIBLE);
        //!hide all

        if (object.getHasSemiCama()) {
            holder.SemiCamaClass.setVisibility(View.VISIBLE);
            holder.SemiCamaLowerPrice.setVisibility(View.VISIBLE);
            holder.SemiCamaPrice.setVisibility(View.VISIBLE);
            holder.SemiCamaPrice.setText(object.getSemiCamaPrice());
        }

        if (object.getHasSalonCama()) {
            holder.SalonCamaClass.setVisibility(View.VISIBLE);
            holder.SalonCamaLowerPrice.setVisibility(View.VISIBLE);
            holder.SalonCamaPrice.setVisibility(View.VISIBLE);
            holder.SalonCamaPrice.setText(object.getSalonCamaPrice());
        }
        if (object.getHasPremium()) {
            holder.PremiumClass.setVisibility(View.VISIBLE);
            holder.PremiumLowerPrice.setVisibility(View.VISIBLE);
            holder.PremiumPrice.setVisibility(View.VISIBLE);
            holder.PremiumPrice.setText(object.getPremiumPrice());
        }

//        imageOperatorLogo

        String Uri = activity.getString(R.string.url_base).replace("/api/v1/", "").concat(object.getObjectBusOperator().getIcon_url());

        activity.getImageLoader().displayImage(Uri, holder.imageOperatorLogo, activity.getOptions(), new SimpleImageLoadingListener() {
            @Override
            public void onLoadingStarted(String imageUri, View view) {
                Log.v("estado", "onLoadingStarted");
            }

            @Override
            public void onLoadingFailed(String s, View view, FailReason failReason) {
                Log.v("estado", "onLoadingFailed");
            }

            @Override
            public void onLoadingComplete(String imageUri, View view, Bitmap loadedImage) {
                Log.v("estado", "onLoadingComplete");
                holder.imageOperatorLogo.setImageBitmap(loadedImage);
//                context.Ocultar();
            }

            @Override
            public void onLoadingCancelled(String imageUri, View view) {
                Log.v("estado", "onLoadingCancelled");
            }
        }, new ImageLoadingProgressListener() {
            @Override
            public void onProgressUpdate(String imageUri, View view, int current, int total) {
                Log.v("estado", "ImageLoadingProgressListener => onProgressUpdate");
            }
        });


        return row;

    }

    static class ContainerSchedule {
        TextView NumberOfServices;
        TextView TravelTimeAverage;
        TextView FirstDeparture;
        TextView LastDeparture;
        TextView SemiCamaClass;
        TextView SemiCamaLowerPrice;
        TextView SemiCamaPrice;
        TextView SalonCamaClass;
        TextView SalonCamaLowerPrice;
        TextView SalonCamaPrice;
        TextView PremiumClass;
        TextView PremiumLowerPrice;
        TextView PremiumPrice;

        ImageView imageOperatorLogo;

    }
}
