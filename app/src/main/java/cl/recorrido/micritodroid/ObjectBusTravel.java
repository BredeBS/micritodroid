package cl.recorrido.micritodroid;

import java.util.ArrayList;
import java.util.Date;

/**
 * Created by brede on 04-11-14.
 */
public class ObjectBusTravel {
    int id;
    ArrayList<ObjectBusSchedule> Schedule;
    ArrayList<ObjectBusOperatorSchedule> BusOperator;
    Date DepartureDate;

    public ArrayList<ObjectBusOperatorSchedule> getBusOperator() {
        return BusOperator;
    }

    public void setBusOperator(ArrayList<ObjectBusOperatorSchedule> busOperator) {
        BusOperator = busOperator;
    }

    Date ReturnDate;
    boolean RoundTrip;
    int Aggregator;

    public ObjectBusTravel(int id, ArrayList<ObjectBusSchedule> schedule, Date departureDate, Date returnDate, boolean roundTrip, ObjectCity destinationCity, ObjectCity departureCity, String search_progress_url) {
        this.id = id;
        Schedule = schedule;
        DepartureDate = departureDate;
        ReturnDate = returnDate;
        RoundTrip = roundTrip;
        DestinationCity = destinationCity;
        DepartureCity = departureCity;
        this.search_progress_url = search_progress_url;
    }

    ObjectCity DestinationCity;
    ObjectCity DepartureCity;

    String search_progress_url;

    public Date getReturnDate() {
        return ReturnDate;
    }

    public void setReturnDate(Date returnDate) {
        ReturnDate = returnDate;
    }

    public int getAggregator() {
        return Aggregator;
    }

    public void setAggregator(int aggregator) {
        Aggregator = aggregator;
    }

    public String getSearch_progress_url() {

        return search_progress_url;
    }

    public void setSearch_progress_url(String search_progress_url) {
        this.search_progress_url = search_progress_url;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public ArrayList<ObjectBusSchedule> getSchedule() {
        return Schedule;
    }

    public void setSchedule(ArrayList<ObjectBusSchedule> schedule) {
        Schedule = schedule;
    }

    public Date getDepartureDate() {
        return DepartureDate;
    }

    public void setDepartureDate(Date departureDate) {
        DepartureDate = departureDate;
    }

    public boolean isRoundTrip() {
        return RoundTrip;
    }

    public void setRoundTrip(boolean roundTrip) {
        RoundTrip = roundTrip;
    }

    public ObjectCity getDestinationCity() {
        return DestinationCity;
    }

    public void setDestinationCity(ObjectCity destinationCity) {
        DestinationCity = destinationCity;
    }

    public ObjectCity getDepartureCity() {
        return DepartureCity;
    }

    public void setDepartureCity(ObjectCity departureCity) {
        DepartureCity = departureCity;
    }

    public ObjectBusTravel() {

    }


}
