package cl.recorrido.micritodroid;


import android.app.ProgressDialog;
import android.content.Context;
import android.content.SharedPreferences;
import android.graphics.drawable.Drawable;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.os.StrictMode;
import android.support.v4.app.Fragment;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.AbsListView;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.CalendarView;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.TextView;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Calendar;


/**
 * A simple {@link Fragment} subclass.
 */
public class FragmentSearch extends Fragment {

    //    int = 0;
    ActivityMain activity;
    Util u;
    ImageView imageDeparture, imageDestination;
    Connection cn = new Connection();
    AdapterCity DepartureAdapter, DestinyAdapter;
    AdapterCountry CountryDestinationAdapter, CountryDepartureAdapter;
    LinearLayout ModalDeparture, ModalDestiny, ModalCalendar, ModalCountryDestination, ModalCountryDeparture, btnDepartureCountry, btnDestinationCountry, ViewNoConnection, LayoutButtonDateReturn, LayoutButtonDateDeparture, LayoutButtonTo, LayoutButtonFrom, btnCloseDestinyCountry, btnCloseDestinyCity, btnCloseDepartureCountry, btnCloseDepartureCity;
    CalendarView cv;
    ProgressDialog progressDialog, progressDialogHorizontal;

    Calendar calendarToday, calendarTomorrow;
    DB db;
    ObjectState ReloadCities;
    ArrayList<ObjectCity> CitiesDepartureArray, CitiesDestinyArray;
    ArrayList<ObjectCountry> Countries;
    TextView CityDeparture, CityDestiny, TravelDate, TodaySmall, TomorrowSmall, TodayLarge, TomorrowLarge, ReturnSmall, ReturnDate, CountryDeparture, CountryDestination;
    CheckBox returnCbox;
    Handler uiHandler;
    EditText DepartureFilter, DestinyFilter;
    ListView DepartureListView, DestinyListView, DepartureCountryListView, DestinationCountryListView;
    Button btnSearch;

    public FragmentSearch() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_search, container, false);
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        activity = (ActivityMain) getActivity();
        u = new Util();
        calendarToday = Calendar.getInstance();
        calendarToday.set(Calendar.MINUTE, 0);
        calendarToday.set(Calendar.HOUR, 0);
        calendarToday.set(Calendar.SECOND, 0);
        calendarTomorrow = Calendar.getInstance();
        calendarTomorrow.add(Calendar.DAY_OF_MONTH, 1);


        //revisar


        btnSearch = (Button) activity.findViewById(R.id.btnSearch);
        uiHandler = new Handler();
        cv = (CalendarView) activity.findViewById(R.id.Calendar);
        //views
        ModalDeparture = (LinearLayout) activity.findViewById(R.id.ViewDeparture);
        ModalDestiny = (LinearLayout) activity.findViewById(R.id.ViewDestiny);
        ModalCalendar = (LinearLayout) activity.findViewById(R.id.ViewCalendar);
        ModalCountryDestination = (LinearLayout) activity.findViewById(R.id.ViewCountryDestination);
        ModalCountryDeparture = (LinearLayout) activity.findViewById(R.id.ViewCountryDeparture);
        btnDepartureCountry = (LinearLayout) activity.findViewById(R.id.DepartureCountry);
        btnDestinationCountry = (LinearLayout) activity.findViewById(R.id.DestinationCountry);
        ViewNoConnection = (LinearLayout) activity.findViewById(R.id.ViewNoConnection);

        LayoutButtonDateReturn = (LinearLayout) activity.findViewById(R.id.LayoutButtonDateReturn);
        LayoutButtonDateDeparture = (LinearLayout) activity.findViewById(R.id.LayoutButtonDateDeparture);
        LayoutButtonTo = (LinearLayout) activity.findViewById(R.id.LayoutButtonTo);
        LayoutButtonFrom = (LinearLayout) activity.findViewById(R.id.LayoutButtonFrom);
        btnCloseDestinyCountry = (LinearLayout) activity.findViewById(R.id.btnCloseDestinyCountry);
        btnCloseDestinyCity = (LinearLayout) activity.findViewById(R.id.btnCloseDestinyCity);
        btnCloseDepartureCountry = (LinearLayout) activity.findViewById(R.id.btnCloseDepartureCountry);
        btnCloseDepartureCity = (LinearLayout) activity.findViewById(R.id.btnCloseDepartureCity);

        //
        ModalDeparture.setVisibility(View.INVISIBLE);
        ModalDestiny.setVisibility(View.INVISIBLE);
        ModalCalendar.setVisibility(View.INVISIBLE);
        ViewNoConnection.setVisibility(View.INVISIBLE);
        ModalCountryDestination.setVisibility(View.INVISIBLE);
        ModalCountryDeparture.setVisibility(View.INVISIBLE);

        imageDeparture = (ImageView) activity.findViewById(R.id.imageDeparture);
        imageDestination = (ImageView) activity.findViewById(R.id.imageDestination);
        //

        CityDeparture = (TextView) activity.findViewById(R.id.comunaOrigen);
        CityDestiny = (TextView) activity.findViewById(R.id.ComunaDestino);
        TravelDate = (TextView) activity.findViewById(R.id.TravelDate);
        TodaySmall = (TextView) activity.findViewById(R.id.TodaySmall);
        TodayLarge = (TextView) activity.findViewById(R.id.TodayLarge);
        TomorrowSmall = (TextView) activity.findViewById(R.id.TomorrowSmall);
        TomorrowLarge = (TextView) activity.findViewById(R.id.TomorrowLarge);
        CountryDeparture = (TextView) activity.findViewById(R.id.txtCountryDeparture);
        CountryDestination = (TextView) activity.findViewById(R.id.txtCountryDestination);
        //return
        ReturnSmall = (TextView) activity.findViewById(R.id.ReturnSmall);
        ReturnSmall.setVisibility(View.INVISIBLE);
        LayoutButtonDateReturn.setVisibility(View.INVISIBLE);
        ReturnDate = (TextView) activity.findViewById(R.id.ReturnDate);
        ReturnDate.setVisibility(View.INVISIBLE);
        returnCbox = (CheckBox) activity.findViewById(R.id.returnCbox);

        returnCbox.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (returnCbox.isChecked()) {
                    ReturnSmall.setVisibility(View.VISIBLE);
                    LayoutButtonDateReturn.setVisibility(View.VISIBLE);
                    ReturnDate.setVisibility(View.VISIBLE);
                    activity.IsRoundTrip = true;
                } else {
                    ReturnSmall.setVisibility(View.INVISIBLE);
                    LayoutButtonDateReturn.setVisibility(View.INVISIBLE);
                    ReturnDate.setVisibility(View.INVISIBLE);
                    activity.IsRoundTrip = false;
                }
            }
        });
        //!return
        DepartureCountry();
        DestinationCountry();
        TomorrowLarge.setText(u.CalendarToString(calendarTomorrow));
        TodayLarge.setText(u.CalendarToString(calendarToday));
        TravelDate.setText(u.CalendarToString(calendarToday));
        TodaySmall.setOnClickListener(ChangeDateAndClose(calendarToday));
        TodayLarge.setOnClickListener(ChangeDateAndClose(calendarToday));
        TomorrowSmall.setOnClickListener(ChangeDateAndClose(calendarTomorrow));
        TomorrowLarge.setOnClickListener(ChangeDateAndClose(calendarTomorrow));

        DepartureFilter = (EditText) activity.findViewById(R.id.DepartureFilter);
        DestinyFilter = (EditText) activity.findViewById(R.id.DestinyFilter);
        //Departure
        LayoutButtonFrom.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                CitiesFilter("");
                ModalDeparture.setVisibility(View.VISIBLE);
                activity.setTitleActionBar(activity.getText(R.string.SelectCityDeparture).toString());
            }
        });
        CityDeparture.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                CitiesFilter("");
                ModalDeparture.setVisibility(View.VISIBLE);
                activity.setTitleActionBar(activity.getText(R.string.SelectCityDeparture).toString());
            }
        });
        btnDepartureCountry.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
//                CitiesFilter("");
                ModalCountryDeparture.setVisibility(View.VISIBLE);
                activity.setTitleActionBar(activity.getText(R.string.SelectCountryDeparture).toString());
            }
        });
        btnDestinationCountry.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
//                CitiesFilter("");
                ModalCountryDestination.setVisibility(View.VISIBLE);
                activity.setTitleActionBar(activity.getText(R.string.SelectCountryDestination).toString());
            }
        });
        DepartureFilter.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
//                CitiesFilter(DepartureFilter.getText().toString());
            }

            @Override
            public void afterTextChanged(Editable s) {
                CitiesFilter(DepartureFilter.getText().toString());
            }
        });
//        cv.setOnClickListener(DetectDateAndClose());

        cv.setOnDateChangeListener(new CalendarView.OnDateChangeListener() {

            @Override
            public void onSelectedDayChange(CalendarView view, int year, int month,
                                            int dayOfMonth) {
//                month += 1;
                if (activity.FieldDate == 1) {
                    //fix: add 1 month to
                    activity.DateDeparture.set(year, month, dayOfMonth, 0, 0, 0);
                    TravelDate.setText(u.CalendarToString(activity.DateDeparture, "dd/MM/yyyy"));
                } else {
                    activity.DateReturn.set(year, month, dayOfMonth, 23, 59, 59);
//                    activity.ReturnYear = year;
//                    activity.ReturnMonth = month + 1;
//                    activity.ReturnDay = dayOfMonth;
                    ReturnDate.setText(u.CalendarToString(activity.DateReturn, "dd/MM/yyyy"));
                }
                DateCompare();
            }
        });
        //close modal
        btnCloseDestinyCountry.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                CleanTitle();
                ModalCountryDestination.setVisibility(View.INVISIBLE);
            }
        });
        btnCloseDestinyCity.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ModalDestiny.setVisibility(View.INVISIBLE);
                CleanTitle();
            }
        });
        btnCloseDepartureCountry.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ModalCountryDeparture.setVisibility(View.INVISIBLE);
                CleanTitle();
            }
        });
        btnCloseDepartureCity.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ModalDeparture.setVisibility(View.INVISIBLE);
                CleanTitle();
            }
        });
        //!close modal
        //Destiny
        LayoutButtonTo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                DestinyFilterFx("");
                ModalDestiny.setVisibility(View.VISIBLE);
                activity.setTitleActionBar(activity.getText(R.string.SelectCityDestination).toString());
            }
        });
        CityDestiny.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                DestinyFilterFx("");
                ModalDestiny.setVisibility(View.VISIBLE);
                activity.setTitleActionBar(activity.getText(R.string.SelectCityDestination).toString());
            }
        });


        DestinyFilter.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
//                DestinyFilterFx(DestinyFilter.getText().toString());
            }

            @Override
            public void afterTextChanged(Editable s) {
                DestinyFilterFx(DestinyFilter.getText().toString());
            }
        });
        ViewNoConnection.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                isConnectionAvailable();
            }
        });
        //!Destiny

        LayoutButtonDateDeparture.setOnClickListener(CalendarShow(1));
        LayoutButtonDateReturn.setOnClickListener(CalendarShow(2));
        TravelDate.setOnClickListener(CalendarShow(1));
        ReturnDate.setOnClickListener(CalendarShow(2));
        //btnSearch Click
        btnSearch.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                activity.SearchTravel();
                CityDestiny.setText(R.string.Select);
            }
        });
        //!btnSearch Click

        isConnectionAvailable();

    }

    public void isConnectionAvailable() {
        if (!u.isNetworkAvailable(activity.getBaseContext())) {
            ViewNoConnection.setVisibility(View.VISIBLE);
        } else {
            ViewNoConnection.setVisibility(View.INVISIBLE);
//            RetrieveInfoFromAPI();//First connection to the API
            CitiesFilter("");

            //TODO: DELETE IT
//            activity.IsRoundTrip=true;
            activity.CityDepartureId = 9333;
            activity.CityDestinyId = 9363;

//            activity.DateDeparture.set(2014, 10, 20, 0, 0, 0);
//            activity.DateReturn.set(2014, 10, 27, 23, 59, 59);

//            activity.SearchTravel();
//            !DELETE IT
        }
    }


    public View.OnClickListener CalendarShow(int type) {
        final int typeTmp = type;

        return new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                activity.FieldDate = typeTmp;
                Calendar calendarMax = Calendar.getInstance();
                calendarMax.add(Calendar.MONTH, 1);
                long dateMax = calendarMax.getTime().getTime();
                cv.setMaxDate(dateMax);
                long dateMin = calendarToday.getTime().getTime();
                cv.setDate(dateMin);
                cv.setMinDate(dateMin);
                cv.setShowWeekNumber(false);
                ModalCalendar.setVisibility(View.VISIBLE);
                activity.IsOpened = 1;
                if (typeTmp == 1) {
                    activity.setTitleActionBar(activity.getText(R.string.SelectDepartureDate).toString());
                } else if (typeTmp == 2) {
                    activity.setTitleActionBar(activity.getText(R.string.SelectReturnDate).toString());
                }

            }
        };

    }

    public void CleanTitle() {
        activity.setTitleActionBar(activity.getBaseContext().getText(R.string.titleSearch).toString());
    }

    public void CitiesFilter(final String texto) {
//        final String textob =texto.trim();
        DepartureListView = (ListView) activity.findViewById(R.id.DepartureContainer);
        DepartureListView.setVisibility(View.VISIBLE);
        db = new DB(activity);
        if (texto.trim().length() != 0) {
            CitiesDepartureArray = db.CityListFilter(activity.DepartureCountryID, texto.trim());
        } else {
            CitiesDepartureArray = db.CityListAll(activity.DepartureCountryID);
        }
        if (CitiesDepartureArray.size() > 0) {
            DepartureAdapter = new AdapterCity(activity, R.layout.extra_fila_city, CitiesDepartureArray);
            DepartureListView.setClickable(false);
            DepartureListView.setAdapter(DepartureAdapter);
            DepartureListView.setChoiceMode(AbsListView.CHOICE_MODE_SINGLE);

            DepartureListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                @Override
                public void onItemClick(AdapterView<?> parent, View view, int position,
                                        long id) {
                    ObjectCity city = CitiesDepartureArray.get(position);
                    Log.v("aaa", String.valueOf(city.getId()));
                    CityDeparture.setText(city.getName());
                    InputMethodManager imm = (InputMethodManager) activity.getSystemService(
                            Context.INPUT_METHOD_SERVICE);
                    imm.hideSoftInputFromWindow(DepartureFilter.getWindowToken(), 0);
                    ModalDeparture.setVisibility(View.INVISIBLE);
                    ModalDestiny.setVisibility(View.INVISIBLE);
                    activity.CityDepartureId = city.getId();
                    try {
                        new AsyncTask<Void, Void, Void>() {
                            @Override
                            protected void onPreExecute() {
                                if (progressDialog == null) {
                                    progressDialog = new ProgressDialog(activity);
                                    progressDialog.setIndeterminate(true);
                                    progressDialog.setMessage(activity.getBaseContext().getString(R.string.searching_destiny));
                                    progressDialog.show();
                                    progressDialog.setCanceledOnTouchOutside(false);
                                    progressDialog.setCancelable(false);
                                    CityDestiny.setText(R.string.Select);
                                    DestinyFilter.setText("");
                                    ModalDeparture.setVisibility(View.INVISIBLE);
                                    ModalDestiny.setVisibility(View.INVISIBLE);
                                }
                            }

                            @Override
                            protected Void doInBackground(Void... params) {
                                try {

                                    StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
                                    StrictMode.setThreadPolicy(policy);
                                    cn.Destiny(activity.getBaseContext(), activity.getBaseContext().getString(R.string.url_base), String.valueOf(activity.CityDepartureId));

                                } catch (IOException e) {
                                    e.printStackTrace();
                                }
                                return null;
                            }


                            @Override
                            protected void onPostExecute(Void result) {
                                if (progressDialog.isShowing()) {
                                    CityDestiny.setText(R.string.Select);

                                    progressDialog.dismiss();
                                    progressDialog = null;
//                                    ModalDeparture.setVisibility(View.VISIBLE);
                                }
                            }
                        }.execute();
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
            });
        } else {

        }
    }


    public void DepartureCountry() {
        DepartureCountryListView = (ListView) activity.findViewById(R.id.CountryDepartureContainer);

        DepartureCountryListView.setClickable(false);

        db = new DB(activity);
        Countries = db.CountryGetAll();
        if (Countries.size() > 0) {
            activity.DepartureCountryID = Countries.get(0).getCountryId();
            CountryDeparture.setText(Countries.get(0).getCountryName());
        }
        CountryDepartureAdapter = new AdapterCountry(activity, R.layout.extra_row_country, Countries);
        DepartureCountryListView.setAdapter(CountryDepartureAdapter);
        DepartureCountryListView.setChoiceMode(AbsListView.CHOICE_MODE_SINGLE);
        if (Countries.size() > 0) {
            activity.DepartureCountryID = Countries.get(0).getCountryId();
        }
        DepartureCountryListView.setVisibility(View.VISIBLE);
        DepartureCountryListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position,
                                    long id) {
                ObjectCountry country = Countries.get(position);
                CountryDeparture.setText(country.getCountryName());

                InputMethodManager imm = (InputMethodManager) activity.getSystemService(
                        Context.INPUT_METHOD_SERVICE);
                imm.hideSoftInputFromWindow(DepartureFilter.getWindowToken(), 0);
                ModalCountryDeparture.setVisibility(View.INVISIBLE);
                activity.DepartureCountryID = country.getCountryId();

                //
                String uri = "@drawable/" + country.getCountryName().toLowerCase() + "";
                int imageResource = getResources().getIdentifier(uri, null, activity.getPackageName());
                Drawable res = getResources().getDrawable(imageResource);
                imageDeparture.setImageDrawable(res);
                //!

//                imageDestination.setBackground(getBaseContext().getDrawable(R.drawable));
                CityDestiny.setText(R.string.Select);
                activity.CityDestinyId = 0;
                CityDeparture.setText(R.string.Select);
                activity.CityDepartureId = 0;
            }
        });

    }

    public void DestinationCountry() {
        DestinationCountryListView = (ListView) activity.findViewById(R.id.CountryDestinationContainer);

        DestinationCountryListView.setClickable(false);

        db = new DB(activity);

        Countries = db.CountryGetAll();
        if (Countries.size() > 0) {
            activity.DestinationCountryID = Countries.get(0).getCountryId();
            CountryDestination.setText(Countries.get(0).getCountryName());
        }
        CountryDestinationAdapter = new AdapterCountry(activity, R.layout.extra_row_country, Countries);
        DestinationCountryListView.setAdapter(CountryDestinationAdapter);
        DestinationCountryListView.setChoiceMode(AbsListView.CHOICE_MODE_SINGLE);

        DestinationCountryListView.setVisibility(View.VISIBLE);
        DestinationCountryListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position,
                                    long id) {
                ObjectCountry country = Countries.get(position);
                CountryDestination.setText(country.getCountryName());
                InputMethodManager imm = (InputMethodManager) activity.getSystemService(
                        Context.INPUT_METHOD_SERVICE);
                imm.hideSoftInputFromWindow(DestinyFilter.getWindowToken(), 0);
                ModalCountryDestination.setVisibility(View.INVISIBLE);
                activity.DestinationCountryID = country.getCountryId();
                CityDestiny.setText(R.string.Select);
                activity.CityDestinyId = 0;
                String uri = "@drawable/" + country.getCountryName().toLowerCase() + "";
                int imageResource = getResources().getIdentifier(uri, null, activity.getPackageName());
                Drawable res = getResources().getDrawable(imageResource);
                imageDestination.setImageDrawable(res);
            }
        });

    }

    public void DestinyFilterFx(String texto) {
        DestinyListView = (ListView) activity.findViewById(R.id.DestinyContainer);

        DestinyListView.setClickable(false);

        db = new DB(activity);
        if (texto.length() != 0) {
            CitiesDestinyArray = db.DestinyFilter(activity.DestinationCountryID, String.valueOf(activity.CityDepartureId), texto.trim());
        } else {
            CitiesDestinyArray = db.DestinyListAll(activity.DestinationCountryID, String.valueOf(activity.CityDepartureId));
        }
        DestinyAdapter = new AdapterCity(activity, R.layout.extra_fila_city, CitiesDestinyArray);
        DestinyListView.setAdapter(DestinyAdapter);
        DestinyListView.setChoiceMode(AbsListView.CHOICE_MODE_SINGLE);
        ModalDestiny.setVisibility(View.VISIBLE);
        DestinyListView.setVisibility(View.VISIBLE);
        DestinyListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position,
                                    long id) {
                ObjectCity city = CitiesDestinyArray.get(position);
                Log.v("aaa", String.valueOf(city.getId()));
                CityDestiny.setText(city.getName());
                InputMethodManager imm = (InputMethodManager) activity.getSystemService(
                        Context.INPUT_METHOD_SERVICE);
                imm.hideSoftInputFromWindow(DestinyFilter.getWindowToken(), 0);
                ModalDestiny.setVisibility(View.INVISIBLE);
                activity.CityDestinyId = city.getId();

            }
        });

    }

//    @Override
//    public boolean onCreateOptionsMenu(Menu menu) {
//        // Inflate the menu; this adds items to the action bar if it is present.
//        getMenuInflater().inflate(R.menu.menu_activity_search, menu);
//        return true;
//    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }


    public View.OnClickListener ChangeDateAndClose(Calendar date) {
        final Calendar dateFinal = date;
        return new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                cv.setDate(dateFinal.getTime().getTime());

                if (activity.FieldDate == 1) {
                    TravelDate.setText(u.CalendarToString(dateFinal));
                } else {
                    ReturnDate.setText(u.CalendarToString(dateFinal));
                }
                DateCompare();
            }
        };
    }

    public void DateCompare() {
        if (activity.DateCompare()) {
            activity.Message(activity.getBaseContext().getText(R.string.ReturnAfterTrip).toString());
        } else {
            ModalCalendar.setVisibility(View.INVISIBLE);
            CleanTitle();
        }
    }

}
